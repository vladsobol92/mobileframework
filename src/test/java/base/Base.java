package base;

import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.List;
import static core.Log.log;

public class Base {
    protected static final String USER_HOME = System.getProperty("user.home");
    protected static final String USER_DIR = System.getProperty("user.dir");


    final String TAG = "    Base():                         | "; // 35 + |

    public boolean compareListsContent(List actual, List expected) {
        log("       compareListsContent(): ");
        boolean listsEqual = false;
        if (actual.size() != expected.size()) {
            log("       compareListsContent(): Lists size is NOT equal");
            return false;
        }
        for (Object obj : expected) {
            if (actual.contains(obj)) {
                listsEqual = true;
            } else {
                log("       compareListsContent(): actual list NOT contain value '" + obj.toString() + "' from expected list ");
                listsEqual = false;
                break;
            }
        }
        return listsEqual;
    }


    public String getDateValue(String dateType, int index) {
        DateFormatSymbols dfs = new DateFormatSymbols();
        String[] monthName = dfs.getMonths();

        String result = "";
        Calendar c = Calendar.getInstance();

        if (dateType.contains("year")) {
            result = String.valueOf(c.get(Calendar.YEAR) + index); // -1 previous year / 0 current year / +1 future year
        } else if (dateType.contains("month")) {
            int monthIndex = c.get(Calendar.MONTH) + index; // -1 previous month / 0 current month / +1 future month
            result = monthName[monthIndex];
        } else if (dateType.contains("day")) {
            result = String.valueOf(c.get(Calendar.DAY_OF_MONTH) + index); // -1 previous DAY_OF_MONTH / 0 current DAY_OF_MONTH / +1 future DAY_OF_MONTH;
        }
        return result;
    }

    public String getCurrentDateValue(String dateType) {
        return getDateValue(dateType, 0);
    }

    /**
     * returns short name of the month specified
     * getShortMonthName(0) -> returns 'Jan'
     **/
    public String getShortMonthName(int monthNumber) {
        DateFormatSymbols dfs = new DateFormatSymbols();
        String[] months = dfs.getShortMonths();
        return months[monthNumber];
    }


    /*
        E.g. get Integer value  :    getNumberValueFromString(text, Integer.class)
        E.g. get Double value   :     getNumberValueFromString(text, Double.class)
        E.g. get BigDecimal value : getNumberValueFromString(text, BigDecimal.class)
     */
    public Number getNumberValueFromString(Object textValue, Class c, int commaPosition) {
        String text;
        Number value;
        String classType = c.getSimpleName();

        // check textValue
        if (textValue == null)
            text = "";
        else
            text = textValue.toString();

        if (text.isEmpty())
            return null;

        log("       getNumberValueFromString(): '" + text + "', class: " + c.getSimpleName());
        // get number from string
        text = text.replaceAll("[^\\d.,]", "").replaceAll(",", ".");

        // Remove symbol "." in the end of string if text ends with "."
        while (text.endsWith(".")) {
            text = StringUtils.substring(text, 0, text.length() - 1);
        }


        // make only one dot
        int counter = 0;
        do {
            if (text.indexOf(".") == text.lastIndexOf("."))
                break;
            else
                text = text.replaceFirst("\\.", "");
            counter++;
        } while (counter < 5);


        // check dot for ".00" or ".000"
        if (text.indexOf(".") > -1) {
            if (text.split("\\.")[1].length() > commaPosition)
                text = text.replace(".", "");
        }
        switch (classType) {
            case "Integer":
                value = -1;
                try {
                    if (text.contains("."))
                        value = Double.valueOf(text).intValue();
                    else
                        value = Integer.valueOf(text);
                } catch (Exception ex) {
                    log("       getIntegerValue() of text: '" + text + "' FAILED");
                }
                break;
            case "Double":
                value = -1.0;
                try {
                    value = Double.valueOf(text);
                } catch (Exception ex) {
                    log("       getDoubleValue() of text: '" + text + "' FAILED");
                }
                break;
            case "BigDecimal":
                value = new BigDecimal(-1.0);
                try {
                    value = BigDecimal.valueOf(Double.valueOf(text)).setScale(commaPosition, BigDecimal.ROUND_DOWN);
                } catch (Exception ex) {
                    log("       getBigDecimalValue() of text: '" + text + "' FAILED");
                }
                break;
            default:
                value = -1;
        }

        return value;
    }

    public void sleep(int mills){
        try {
            Thread.sleep(mills);
        } catch (InterruptedException e) {
            // ignore
        }
    }

}
