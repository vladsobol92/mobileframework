package base;

import core.MobileGestures;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.qameta.allure.Attachment;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.support.PageFactory;

import java.time.Duration;
import java.util.List;

import static core.Log.log;

public class BaseScreen extends MobileGestures {
    final String TAG = "    BaseScreen():                   | "; // 35 + |
    private int DEFAULT_WAIT_TIME = 20; //default look for elements
    private int FAST_WAIT_TIME = 7; // wait for 7 sec


    public BaseScreen(AppiumDriver driver) {
        super(driver);
        setDefaultWaitTime();
    }

    public String getTAG() {
        return TAG;
    }

    public synchronized void setDefaultWaitTime() {
        PageFactory.initElements(new AppiumFieldDecorator(driver, Duration.ofSeconds(DEFAULT_WAIT_TIME)), this);
    }

    public synchronized void waitFastTime() {
        PageFactory.initElements(new AppiumFieldDecorator(driver, Duration.ofSeconds(FAST_WAIT_TIME)), this);
    }

    public synchronized void setWaitSeconds(int sec) {
        PageFactory.initElements(new AppiumFieldDecorator(driver, Duration.ofSeconds(sec)), this);
    }

    public boolean isScreenLoad(List<MobileElement> element, int sec) {
        log(TAG + "isScreenLoad: ");
        if (sec == -1) {
            setDefaultWaitTime();
        } else {
            setWaitSeconds(sec);
        }
        boolean isLoaded = !element.isEmpty();
        setDefaultWaitTime();
        return isLoaded;
    }

    public boolean isScreenLoad(MobileElement el) {
        boolean bool = false;
        try {
            bool = !el.getId().isEmpty();
        } catch (Exception e) {
        }
        setDefaultWaitTime();
        return bool;
    }

    @Attachment(value = "Page screenshot", type = "image/png")
    public byte[] takeScreenShot() {
        return driver.getScreenshotAs(OutputType.BYTES);
    }

    public String getText(List<MobileElement> el) {
        try {
            return getText(el, 0);
        } catch (Exception ex) {
            return null;
        }

    }

    public String getText(List<MobileElement> el, int num) {
        try {
            return el.get(num).getText();
        } catch (Exception ex) {
            return null;
        }

    }

    public Integer getIntValue(List<MobileElement> el) {
        String text = getText(el);
        try {
            return Integer.valueOf(text.replaceAll("[^\\d]", ""));
        } catch (Exception e) {
            return -1;
        }
    }

    public Double getDoubleValue(List<MobileElement> el) {
        String text = getText(el);
        try {
            return Double.valueOf(text.replaceAll("[^\\d.]", ""));
        } catch (Exception e) {
            return -1.0;
        }
    }

}
