package base;

import static core.Log.log;
import static core.Log.logResults;

import core.appium.AppiumManager;
import core.ServerManager;
import core.recorder.VideoRecorder;
import core.reporter.AllureReporter;
import io.appium.java_client.AppiumDriver;
import io.qameta.allure.Step;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.*;
import org.testng.annotations.Optional;
import org.testng.asserts.SoftAssert;


import java.lang.reflect.Method;
import java.util.*;

public class BaseTestParallel extends Base{

    static HashMap<String,AppiumManager> appiumManagerList;


    AllureReporter allureReporter;
    static final String result="src/target/allure-results";

    //protected AppiumDriver driver;
    public ThreadLocal <AppiumDriver> driverThLocal = new ThreadLocal<AppiumDriver>();
    public ThreadLocal <String> device = new ThreadLocal<String>();
    public ThreadLocal <VideoRecorder> videoRecorder = new ThreadLocal<VideoRecorder>();
    public ThreadLocal <SoftAssert> softAssert = new ThreadLocal<SoftAssert>();
    public static List<String> devicePool;
    public static List<String> takenDevices;


    @Step("Set up suite {0}")
    @Parameters({"devicePlatform","device","results"})
    @BeforeSuite(alwaysRun = true)
    public  void beforeSuite(@Optional("")String devicePlatform,
                             @Optional("") String device,
                             @Optional(result)String results,
                             ITestContext context){
        log("       beforeSuite():");

        appiumManagerList = new HashMap<>();
        takenDevices = new ArrayList<String>();
        setDevicePool(device); //create pool of devices

        for (String deviceName : devicePool) {
            appiumManagerList.put(deviceName, new AppiumManager(deviceName)); // create new AppiumManager() object for every device
        }
        for (AppiumManager manager : appiumManagerList.values() ){
            manager.startAppiumServer();
        }

        context.getSuite().getXmlSuite().setThreadCount(devicePool.size()); // set thread-count according to devices count
        log("This is thread-count "+context.getSuite().getXmlSuite().getThreadCount());
        context.getSuite().getXmlSuite().setDataProviderThreadCount(devicePool.size()); // set thread-count fro dataprovider according to devices count
        allureReporter = new AllureReporter(devicePool,devicePlatform);
        allureReporter.createReportDirectory();
    }

    @Step("Start driver {0}")
    @Parameters({"device", "clear"})
    @BeforeMethod(alwaysRun = true)
    public void beforeMethod(@Optional("") String device, Method m, @Optional("false") String clear){
        log("       beforeMethod() " + m.getName());

        sleep(new Random().nextInt(1000)); // TODO try to remove
        setTLDevice(takeDeviceFromList(devicePool));

        log("This device: " + getTLDevice());

        log("+++++++++++++ START TEST ++++++++++++++");
        log("+++++++++ "+m.getName()+" +++++++++");
        softAssert.set(new SoftAssert());
        DesiredCapabilities capabilities = appiumManagerList.get(getTLDevice()).capabilities(clear);

        setTLDriver(appiumManagerList.get(getTLDevice()).mobileDriver(capabilities));
        setVideoRecorder(new VideoRecorder(m.getName(),getTLDevice(), driver()));

        try {
            videorecorder().startRecording(capabilities.getCapability("platformName").toString(),
                    appiumManagerList.get(getTLDevice()).getDeviceUdid(getTLDevice()));
        } catch (Exception ex){
            log("       beforeMethod(): Video record start FAILED\n"+ex.getMessage());
            ex.printStackTrace();

        }

    }

    @Step("Stop driver")
    @AfterMethod(alwaysRun = true)
    public void afterMethod(ITestResult result, ITestContext context){
        try {
            videorecorder().stopRecording(getTLDevice() + "_" + result.getMethod().getMethodName() + "_" + System.currentTimeMillis(), result);
            videorecorder().attachVideo();
            videorecorder().deleteVideo(); // delete video from machine. Will only keep it inside Allure reporter
        } catch (Exception ex) {
            log("   Exception occured while stoppping video \n" + ex.getMessage());
        }

        log("+++++++++++++ FINISH ++++++++++++++");
        logResults(context,result);
        log("_______________________________________\n");

        log("  close driver:");
        quitDriver(driver());

        log("  free Device: " + getTLDevice());
        freeDevice(getTLDevice());

    }

    @Step("Generate report")
    @AfterSuite(alwaysRun = true)
    public void afterSuite(){
        log("afterSuite()");
        for (String deviceName : appiumManagerList.keySet()) {
            appiumManagerList.get(deviceName).stopAppiumServer(); // stop appium server for each device
        }
        allureReporter.createEnvironment();
        allureReporter.generateAllureReport();
    }


    private synchronized void setTLDevice(String device) {
        this.device.set(device);
    }

    public synchronized String getTLDevice() {
        return this.device.get();
    }


    private synchronized void setTLDriver(AppiumDriver driver) {
        driverThLocal.set(driver);
    }

    public synchronized AppiumDriver driver() {
        return driverThLocal.get();
    }


    private synchronized void setVideoRecorder(VideoRecorder videoRecorder) {
       this.videoRecorder.set(videoRecorder);
    }

    public synchronized VideoRecorder videorecorder() {
        return videoRecorder.get();
    }



    public void setDevicePool(String device){
        log("setDevicePool()");

        Long startTime = System.currentTimeMillis();
        devicePool = new LinkedList<String>(Arrays.asList(device.split(",")));

        log("setDevicePool(): SIZE :" +devicePool.size());

        for (String deviceName : devicePool){
            boolean isConnected = ServerManager.isDeviceConnected(deviceName); // verify each device is connected
            if (!isConnected){
                log("setDevicePool(): DEVICE "+deviceName+" is NOT connected");
                devicePool.remove(deviceName);
            }
        }
        log("setDevicePool(): SIZE :" +devicePool.size());

        log("setDevicePool() DONE in: "+(System.currentTimeMillis() - startTime)+" mills");
    }
    /*
    @Attachment(value = "device")
    @Step("Device")
    private synchronized String getDeviceName(){
        return device;
    }
    */


    private synchronized void quitDriver(AppiumDriver driver){
        try {
            if(driver!=null) {
                driver.quit();
                log("  close driver: DONE");
            } else {log("  close driver: Already CLOSED");}
        } catch (Exception ex){
            log("       quitDriver(): FAILED\n"+ex.getMessage());
        }

    }

    private synchronized String takeDeviceFromList(List<String> devicePool) {
        String deviceToTake = null;

        for ( String device : devicePool) {
            if(!takenDevices.contains(device)) {
                deviceToTake = device;
                log("takeDeviceFromList(): device taken SUCCESS "+device);
                takenDevices.add(deviceToTake);
                break;
            } else {
                log("takeDeviceFromList(): DEVICE CANT BE TAKEN "+device);
            }
        }
        log("takeDeviceFromList(): device taken " + deviceToTake);
        return deviceToTake;
    }


    public synchronized void freeDevice(String device){
        log("freeDevice() "+device);
        boolean deviceIsFree = false;
        int attempts = 0;
        try {
            do {
                for (Iterator<String> iterator = takenDevices.iterator(); iterator.hasNext(); ) {
                    log("freeDevice() iterating list...");
                    if (iterator.next().contains(device)) {
                        iterator.remove();
                        log("Device is REMOVED " + device);
                    }
                }
                deviceIsFree = !takenDevices.contains(device);
                attempts++;
                log("freeDevice() WHILE");
                log("Devices in List of taken devices: " + takenDevices);
            } while (!deviceIsFree && attempts < 5);
        } catch ( Exception e){
            log("freeDevice() "+device);
            log(e.getMessage());
        }

    }

}
