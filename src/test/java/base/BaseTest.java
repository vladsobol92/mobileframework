package base;

import static core.Log.log;
import static core.Log.logResults;

import core.appium.AppiumManager;
import core.ServerManager;
import core.recorder.VideoRecorder;
import core.reporter.AllureReporter;
import io.appium.java_client.AppiumDriver;
import io.qameta.allure.Step;
import lombok.Getter;
import lombok.Setter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.*;


import java.lang.reflect.Method;

public class BaseTest extends Base {

    static AppiumManager appiumManager;
    DesiredCapabilities capabilities;
    protected AppiumDriver driver;
    @Getter@Setter
    String device;
    VideoRecorder videoRecorder;
    AllureReporter allureReporter;
    static final String result="src/target/allure-results";

    @Step("Set up suite {0}")
    @Parameters({"device","results"})
    @BeforeSuite(alwaysRun = true)
    public void beforeSuite(@Optional("") String device, @Optional(result)String results){
        log("beforeSuite():");
        this.device=device;
        ServerManager.verifyDeviceConnectivity(device);
        appiumManager = new AppiumManager(device);
        appiumManager.startAppiumServer();
        allureReporter = new AllureReporter();
        allureReporter.createReportDirectory();

    }
    @Step("Start driver {0}")
    @Parameters({"device", "clear"})
    @BeforeMethod(alwaysRun = true)
    public void beforeMethod(@Optional("") String device, Method m, @Optional("false") String clear){
        log("beforeMethod()");
        this.device=device;
        ServerManager.verifyDeviceConnectivity(device);
        log("+++++++++++++ START TEST ++++++++++++++");
        log("+++++++++ "+m.getName()+" +++++++++");
        DesiredCapabilities capabilities = appiumManager.capabilities(clear);
        driver = appiumManager.mobileDriver(capabilities);
        videoRecorder = new VideoRecorder(m.getName(),this.device, driver);
        videoRecorder.startRecording(capabilities.getCapability("platformName").toString(),
                appiumManager.getDeviceUdid(device));

    }

    @Step("Stop driver")
    @AfterMethod(alwaysRun = true)
    public void afterMethod(ITestResult result, ITestContext context){
        videoRecorder.stopRecording(getDevice() + "_" + result.getMethod().getMethodName() + "_" + System.currentTimeMillis(), result);

        if (!result.isSuccess()) {
            videoRecorder.attachVideo();
        }
        videoRecorder.deleteVideo(); // delete video from machine. Will only keep it inside Allure reporter

        log("+++++++++++++ FINISH ++++++++++++++");
        logResults(context,result);
        log("_______________________________________\n");
        log("  close driver:");
        if(driver!=null) {
            driver.quit();
            log("  close driver: DONE");
        }

    }

    @Step("Generate report")
    @AfterSuite(alwaysRun = true)
    public void afterSuite(){
        log("afterSuite()");
        appiumManager.stopAppiumServer();
        allureReporter.createEnvironment();
        allureReporter.generateAllureReport();
    }

}
