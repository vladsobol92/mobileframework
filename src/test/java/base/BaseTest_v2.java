package base;

import core.Device;
import core.appium.AppiumDriverFactory_v2;
import core.appium.AppiumServer;
import core.utils.logging.Logger;
import edu.emory.mathcs.backport.java.util.Arrays;
import io.appium.java_client.AppiumDriver;
import io.qameta.allure.Step;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.util.ArrayList;
import java.util.List;

import static core.Log.log;

public class BaseTest_v2 extends Base {

    final static String APP_PATH = USER_HOME + "/Documents/test/app/Monese.ipa";
    final static String TEST_ENV = "dev";
    final static int DEVICE_WAIT_TIME = 300000; // 300 sec.

    private static List<AppiumServer> appiumServers = new ArrayList<>();
    private static List<Device> devices = new ArrayList<>(); // all devices requested for test
    private static final List<Device> busyDevices = new ArrayList<>(); // devices that are busy in tests at the moment


    private ThreadLocal<Device> testDevice = new ThreadLocal<>(); // device that is used in test
    private ThreadLocal<AppiumDriver> driver = new ThreadLocal<>(); // driver


    // mvn test -DsuiteName="test.xml" -DdeviceName="iphone" -DstartType="noReset/fullReset/fastReset" -DappiumSaveLogs="false"
    // methods to execute once per suite
    // E.g. start Appium server
    @Parameters({"deviceName", "appiumSaveLogs"})
    @BeforeSuite(alwaysRun = true)
    public void beforeSuite(
            String deviceName,
            @Optional("false") String appiumSaveLogs, ITestContext iTestContext) {
        // TODO add validation for connected devices and start appium server only for connected devices
        initDevices(deviceName);
        for (Device device : devices) {
            log("   beforeSuite: starting appium server for device: " + device.getName());
            AppiumServer appiumServer = new AppiumServer(device);
            appiumServer.startServer(Boolean.parseBoolean(appiumSaveLogs));
            appiumServers.add(appiumServer);
        }
        // set thread count for parallel execution
        iTestContext.getSuite().getXmlSuite().setThreadCount(devices.size());
        iTestContext.getSuite().getXmlSuite().setDataProviderThreadCount(devices.size());
    }

    // methods to execute once per method
    @Parameters({"startType"})
    @BeforeMethod(alwaysRun = true)
    public void beforeMethod(@Optional("noReset") String startType) {
        log("   beforeMethod()");
        // Take device from the list of available device
        Device device = takeDeviceForTest(DEVICE_WAIT_TIME);

        // init test device
        setTestDevice(device.setStartType(startType));

        // get Appium server that is running for this device
        AppiumServer server = getRunningAppiumServerForDevice(getTestDevice());
        // Assert server is running for device
        Assert.assertNotNull(server, "  beforeMethod: FAILED to find running Appium Server for: " + getTestDevice().getName());
        // Start driver for test device
        AppiumDriver driver = new AppiumDriverFactory_v2()
                .startDriver(getTestDevice(), TEST_ENV, TEST_ENV, APP_PATH, null, server.getService());
        setDriver(driver);
        log("   beforeMethod(): DONE");
    }


    @AfterMethod(alwaysRun = true)
    public void afterMethod(ITestResult iTestResult, ITestContext iTestContext) {
        // methods to execute after method
        // E.g. kill driver
        log("Test " + iTestResult.getMethod().getTestClass().getName() + ", status " + getResultAsText(iTestResult.getStatus()));
        log("  Execution time: " + (iTestResult.getEndMillis() - iTestResult.getStartMillis()) / 1000
                + "sec");


        log("  ---------------------------");
        log("  partially completed results");
        log("   passed  tests: " + iTestContext.getPassedTests().size());
        log("   skipped tests: " + iTestContext.getSkippedTests().size());
        log("   failed  tests: " + iTestContext.getFailedTests().size());


        quitDriver(getDriver());
        releaseDevice(getTestDevice());
    }

    @AfterTest(alwaysRun = true)
    public void afterTest() {
        for (Device device : busyDevices) {
            releaseDevice(device);
        }
    }

    @AfterSuite(alwaysRun = true)
    public void afterSuite() {
        // methods to execute after suite
        // E.g. stop Appium server
        for (AppiumServer server : appiumServers) {
            server.stopServer();
        }
    }

    private void initDevices(String stringToParse) {
        getDeviceNamesList(stringToParse).forEach(name -> devices.add(new Device(name)));

    }

    private List<String> getDeviceNamesList(String stringToParse) {

        return Arrays.asList(stringToParse.split(","));
    }

    /**
     * Go through the list of appium servers and take one that is started for specific device
     *
     * @param device
     * @return
     */
    private AppiumServer getRunningAppiumServerForDevice(Device device) {
        return appiumServers.stream()
                .filter(as -> as.getDevice().getName().equals(device.getName()))
                .findFirst().orElse(null);
    }

    public void setDriver(AppiumDriver driver) {
        this.driver.set(driver);
    }

    public AppiumDriver getDriver() {
        return this.driver.get();
    }

    @Step("Quit driver")
    public synchronized void quitDriver(AppiumDriver driver) {
        Logger.log("   quitDriver()");
        if (driver != null) {
            try {
                driver.quit();
            } catch (Exception e) {
                // ignore
            }
            Logger.log("  close driver: DONE");
        }
    }

    public Device takeDeviceForTest(int waitTime) {
        Device deviceToTake = null;
        synchronized (busyDevices) {
            Logger.log();
            while (busyDevices.size() > 0 && (busyDevices.size() >= devices.size())) {
                try {
                    busyDevices.wait(waitTime);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            for (Device device : devices) {
                if (busyDevices.stream()
                        .noneMatch(d -> d.getName().equals(device.getName()))) {
                    busyDevices.add(device);
                    deviceToTake = device;
                    break;
                }
            }
            busyDevices.notify();
            return deviceToTake;
        }
    }

    private void releaseDevice(Device device) {
        synchronized (busyDevices) {
            // sleep(50);
            log(" freeDevice(): " + device.getName());
            while (busyDevices.stream().anyMatch(d -> d.getName().equals(device.getName()))) {
                busyDevices.remove(device);
            }
            busyDevices.notify();
            log(" freeDevice(): " + device.getName() + " done");
        }
    }


    public void setTestDevice(Device device) {
        this.testDevice.set(device);
    }

    public Device getTestDevice() {
        return this.testDevice.get();
    }


    protected String getResultAsText(int result) {
        switch (result) {
            case 1:
                return "SUCCESS";
            case 2:
                return "FAILURE";
            case 3:
                return "SKIP";
            case 4:
                return "SUCCESS_PERCENTAGE_FAILURE";
            case 16:
                return "STARTED";
            default:
                return "UNKNOWN: " + result;
        }
    }


}
