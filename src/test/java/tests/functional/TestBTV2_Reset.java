package tests.functional;

import base.BaseTest_v2;
import core.utils.logging.Logger;
import org.testng.annotations.Test;


public class TestBTV2_Reset extends BaseTest_v2 {

    @Test
    public void testReset() {
        Logger.log("testReset started");
        sleep(2000);
        String pageSource = getDriver().getPageSource();
        Logger.log(pageSource);
        Logger.log("testReset finished");
    }


}
