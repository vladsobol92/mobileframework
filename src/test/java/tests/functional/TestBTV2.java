package tests.functional;

import base.BaseTest_v2;
import core.utils.logging.Logger;
import org.testng.annotations.Test;


public class TestBTV2 extends BaseTest_v2 {

    @Test
    public void test1() {
        Logger.log("test1 started");
        sleep(2000);
        String pageSource = getDriver().getPageSource();
        Logger.log(pageSource);
        Logger.log("test1 finished");
    }


    @Test
    public void test2() {
        Logger.log("test2 started");
        sleep(5000);
        Logger.log("test2 finished");
    }


    @Test
    public void test3() {
        Logger.log("test3 started");
        sleep(5000);
        Logger.log("test3 finished");
    }


    @Test
    public void test4() {
        Logger.log("test4 started");
        sleep(5000);
        Logger.log("test4 finished");
    }


    @Test
    public void test5() {
        Logger.log("test5 started");
        sleep(5000);
        Logger.log("test5 finished");
    }


    @Test
    public void test6() {
        Logger.log("test6 started");
        sleep(5000);
        Logger.log("test6 finished");
    }


    @Test
    public void test7() {
        Logger.log("test7 started");
        sleep(5000);
        Logger.log("test7 finished");
    }


    @Test
    public void test8() {
        Logger.log("test8 started");
        sleep(5000);
        Logger.log("test8 finished");
    }


    @Test
    public void test9() {
        Logger.log("test9 started");
        sleep(5000);
        Logger.log("test9 finished");
    }


    @Test
    public void test10() {
        Logger.log("test9 started");
        sleep(5000);
        Logger.log("test9 finished");
    }

}
