package tests.functional;

import core.testrail.TestRail;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import screens.example.HomeScreen;
import screens.example.StartScreen;
import base.BaseTestParallel;

import java.util.List;
import java.util.Random;

import static core.Log.log;


public class FIrstTest extends BaseTestParallel {

    final String TAG = "    FIrstTest():                 | "; // 35 + |

    HomeScreen home;
    StartScreen startScreen;

    @Test
    @TestRail(testRailId = "C1")
    public  void testLog1(){
        log(TAG+"testLog1() "+1);
        navigateToStartScreen();
        startScreen.tapLoginButton();
        log(TAG+"This is log: "+1);
        sleep(1000);
        log(TAG+"This is log: "+2);
        sleep(1000);
        log(TAG+"This is log: "+3);
        sleep(1000);
        log(TAG+"This is log: "+4);
        sleep(1000);
        log(TAG+"This is log: "+5);
        startScreen.takeScreenShot();
        softAssert.get().assertAll();
    }



    @Test
    @TestRail(testRailId = "C2")
    public  void testLog2(){
        log("testLog2() "+1);
        navigateToStartScreen();
        startScreen.tapSignUpButton();
        sleep(5000);
        log("This is log: "+2);
        sleep(1000);
        int x = 7;
        int y =  new Random().nextInt(10);
        softAssert.get().assertTrue(x > y,"Random failure x= "+x+" y = "+y+" 'x' is less then 'y'"  );
        softAssert.get().assertAll();
    }

    @Test
    @TestRail(testRailId = "C3")
    public  void testLog3(){
        log(TAG+"testLog3() "+1);
        navigateToStartScreen();
        startScreen.tapLoginButton();
        log(TAG+"This is log: "+1);
        sleep(1000);
        startScreen.takeScreenShot();
        log(TAG+"This is log: "+2);
        sleep(1000);
        log(TAG+"This is log: "+3);
        sleep(1000);
        log(TAG+"This is log: "+4);
        sleep(1000);
        log(TAG+"This is log: "+5);
        startScreen.takeScreenShot();
    }

    @Test
    @TestRail(testRailId = "C4")
    public  void testLog4(){
        log(TAG+"testLog4() "+1);
        navigateToStartScreen();
        startScreen.tapSignUpButton();
        sleep(5000);
        log(TAG+"This is log: "+2);
        sleep(1000);
        int x = 7;
        int y =  new Random().nextInt(10);
        softAssert.get().assertTrue(x > y,"Random failure x= "+x+" y = "+y+" 'x' is less then 'y'"  );
        softAssert.get().assertAll();
    }

    private synchronized void navigateToStartScreen (){
        home = new HomeScreen(driver());
        startScreen = new StartScreen(driver());
        for (int i = 0; i<10; i++){
            List <MobileElement> perms = driver().findElements((By.id("Allow")));
            boolean shownAllowPemrissions = home.isScreenLoad(perms,1);
            boolean startScreenShown = startScreen.isLoaded(1);
            boolean shownLangScreen = startScreen.isLoadedLanguageScreen(1);
            if (shownAllowPemrissions) {
                home.allowPermissionsIOS();
            }
            if (shownLangScreen){
                startScreen.selectDefaultLanguage();
            }
            if(startScreenShown) {
                break;
            }
        }
    }
}
