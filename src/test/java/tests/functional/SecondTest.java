package tests.functional;

import core.testrail.TestRail;
import org.openqa.selenium.By;
import org.testng.annotations.Test;
import screens.example.HomeScreen;
import screens.example.StartScreen;
import base.BaseTestParallel;

import static core.Log.log;

public class SecondTest extends BaseTestParallel {

    final String TAG = "    SecondTest():                 | "; // 35 + |

    @Test(enabled = false)
    @TestRail(testRailId = "C23")
    public void testLog3(){
        log(TAG+"testLog3() "+1);
        StartScreen startScreen = new StartScreen(driver());
        HomeScreen home = new HomeScreen(driver());
        for (int i = 0; i<10; i++){
            boolean shownHome = home.isScreenLoad(driver().findElements(By.id("Allow")),1);
            boolean shownLangScreen = startScreen.isLoadedLanguageScreen(1);
            if (shownHome) {
                home.allowPermissionsIOS();
                break;
            }
            if (shownLangScreen){
                startScreen.selectDefaultLanguage();
                break;
            }
        }

        log(TAG+"This is log: "+1);
        sleep(1000);
        log(TAG+"This is log: "+2);
        sleep(1000);
        log(TAG+"This is log: "+3);
        sleep(1000);
        log(TAG+"This is log: "+4);
    }

    @Test(enabled = false)
    @TestRail(testRailId = "C24")
    public void testLog4(){
        log(TAG+"testLog4() "+1);
        StartScreen startScreen = new StartScreen(driver());
        HomeScreen home = new HomeScreen(driver());
        for (int i = 0; i<10; i++){
            boolean shownHome = home.isScreenLoad(driver().findElements(By.id("Allow")),1);
            boolean shownLangScreen = startScreen.isLoadedLanguageScreen(1);
            if (shownHome) {
                home.allowPermissionsIOS();
                break;
            }
            if (shownLangScreen){
                startScreen.selectDefaultLanguage();
                break;
            }
        }

        log(TAG+"This is log: "+1);
        sleep(1000);
        log(TAG+"This is log: "+2);
        sleep(1000);
        log(TAG+"This is log: "+3);
        sleep(1000);
        log(TAG+"This is log: "+4);
    }

    @Test(enabled = false)
    @TestRail(testRailId = "C25")
    public void testLog5(){
        log(TAG+"testLog3() "+1);
        StartScreen startScreen = new StartScreen(driver());
        HomeScreen home = new HomeScreen(driver());
        for (int i = 0; i<10; i++){
            boolean shownHome = home.isScreenLoad(driver().findElements(By.id("Allow")),1);
            boolean shownLangScreen = startScreen.isLoadedLanguageScreen(1);
            if (shownHome) {
                home.allowPermissionsIOS();
                break;
            }
            if (shownLangScreen){
                startScreen.selectDefaultLanguage();
                break;
            }
        }

        log(TAG+"This is log: "+1);
        sleep(1000);
        log(TAG+"This is log: "+2);
        sleep(1000);
        log(TAG+"This is log: "+3);
    }
}
