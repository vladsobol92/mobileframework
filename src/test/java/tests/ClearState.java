package tests;
import static core.Log.log;

import base.BaseTestParallel;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ClearState extends BaseTestParallel {

    @DataProvider(name = "devices", parallel = true)
    public Iterator<Object[]> createData() {
        List <Object[]> list = new ArrayList<>();
        for (String device : devicePool){
            list.add(new String[]{device});
        }
        return list.iterator();
    }


    @Test(dataProvider = "devices")
    public void clear(String device){
        log("clear(): "+ device);
        log("clear():+++++++++++++++++++++++++++++++++++++++");
    }


    @AfterMethod(alwaysRun = true)
    public void afterClear(){
        log("afterClear(): device is CLEAR");
    }

}
