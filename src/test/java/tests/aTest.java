package tests;

import base.Base;
import core.Device;
import core.ServerManager;
import core.appium.AppiumDriverFactory_v2;
import core.appium.AppiumServer;
import core.utils.logging.Logger;
import io.appium.java_client.AppiumDriver;
import org.testng.annotations.Test;

public class aTest extends Base {


    @Test
    public void appiumServerTest() {

        // Start server

        // parameters for starting driver
        String deviceName = "iphone7";
        Device device = new Device(deviceName);
        String startType = "fastReset"; // fastReset
        device.setStartType(startType);
        String testEnvironment = "dev";
        String testBuild = "dev";
        String appPath = USER_HOME + "/Documents/test/app/Monese.ipa";
        String processArgument = null;

        AppiumServer appiumServer = new AppiumServer(device);
        appiumServer.startServer(true);


        // start driver
        AppiumDriver driver = new AppiumDriverFactory_v2()
                .startDriver(device,
                        testEnvironment,
                        testBuild,
                        appPath,
                        processArgument,
                        appiumServer.getService());
        Logger.log("Driver started");
        sleep(15000);

        Logger.log("Quitting Driver");
        driver.quit();
        Logger.log("Stoppping Server");
        appiumServer.stopServer();
    }


    @Test
    public void clearPorttest() {
        ServerManager.clearPort(6005);

        // appiumManager_v2.stopServer();
    }
}
