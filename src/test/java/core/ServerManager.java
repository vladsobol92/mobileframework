package core;

import core.appium.AppiumManager;
import org.apache.commons.io.IOUtils;
import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import static core.Log.log;

public class ServerManager {

    private static String OS;
    private static String ANDROID_HOME;

    public static String getOS() {
        if (OS == null)
            OS = System.getenv("os.name");
        return OS;

    }

    public static String getAndroidHome() {
        if (ANDROID_HOME == null) {
            ANDROID_HOME = System.getenv("ANDROID_HOME");
            if (ANDROID_HOME == null) {
                throw new RuntimeException("Failed to find ANDROID_HOME, check if it is set !!!");

            }
        }
        return ANDROID_HOME;

    }


    public static boolean isMac() {
        return getOS().startsWith("Mac");
    }


    public static String runCommand(String command) {
        log("  runCommand(): " + command);
        String output = "";
        try {
            InputStream in = Runtime.getRuntime().exec(command).getInputStream();
            Scanner scanner = new Scanner(in).useDelimiter("\\A");
            if (scanner.hasNext()) {
                log("  runCommand():scanner.hasNext ");
                output = scanner.next();
            }
        } catch (IOException ex) {
            throw new RuntimeException(ex.getMessage());

        }
        return output;

    }

    public static String runCommand(String[] cmd) {
        log("        runCommand: " + Arrays.toString(cmd));
        String stdout = null;
        try {
            final Process p = new ProcessBuilder(cmd)
                    .redirectErrorStream(true)
                    // .redirectOutput(ProcessBuilder.Redirect.INHERIT)
                    .start();
            p.waitFor(60, TimeUnit.SECONDS);
            stdout = IOUtils.toString(p.getInputStream(), Charset.defaultCharset());
            p.destroy();
            // log(stdout);
        } catch (Exception ex) {
            log("        runCommand: FAILED\n" + ex.getMessage());
        }
        return stdout;
    }

    // clear up port
    public static <T> void clearPort(T port) {
        log("  clearPort(): " + port);
        String output = runCommand("lsof -i :" + port + " -t"); // this command returns the ID of running process on the port
        log("  clearPort(): output: " + output);
        if (!output.isEmpty()) {
            String[] process_ids = output.split("\n");
            boolean isFree = process_ids.length < 1;
            if (!isFree) {
                for (String id : process_ids) {
                    runCommand("kill " + id);
                    log("  clearPort(): DONE");
                }
            }
        } else {
            log("clearPort(): Output is empty, port should be FREE");
        }

    }

    public static ArrayList getRecordingProcesses(String deviceUdid) {
        String[] output = runCommand("ps -ax").split("\n");
        ArrayList proceses = new ArrayList();
        for (String line : output) {
            if (line.contains("record") && line.contains(deviceUdid)) {
                proceses.add(line.trim().split(" ")[0]);
                proceses.removeAll(Arrays.asList("", null));
                log("  getRecordingProcesses(): " + line.split(" ")[0].toString());
            }
        }
        if (proceses.size() < 1) {
            log("   getRecordingProcesses(): NO recording processes for device " + deviceUdid);
        }

        return proceses;
    }


    public static boolean isDeviceConnected(String deviceName) {
        log("  isDeviceConnected(): ");
        JSONObject deviceData = (JSONObject) AppiumManager.readCapabilitiesFile().get(deviceName);
        //String name = (String) deviceData.get("deviceName");
        String platform = "";
        String udid = "";
        String name = "";
        try {
            platform = (String) deviceData.get("platformName");
            udid = (String) deviceData.get("udid");
            name = (String) deviceData.get("deviceName");
        } catch (NullPointerException ex) {
            throw new RuntimeException("No such device in capabilities file: " + deviceName);
        }

        boolean isConnected = false;

        if (!deviceName.contains("sim")) {

            for (Object device : getConnectedDevices(platform)) {
                if (device.toString().equals(udid)) {
                    isConnected = true;
                    break;
                }
            }
            return isConnected;
        } else {
            for (HashMap device : getAvailableSimulatorsIOS()) {
                if (device.get("name").equals(name)) {
                    isConnected = true;
                    break;
                }
            }
        }
        return isConnected;
    }

    public static void verifyDeviceConnectivity(String deviceName) {
        log("  verifyDeviceConnectivity(): ");
        if (!ServerManager.isDeviceConnected(deviceName)) {
            throw new RuntimeException("Device " + deviceName + " is NOT connected");
        }
    }

    private static ArrayList getConnectedDevices(String platform) {

        ArrayList devices = new ArrayList();

        if (platform.equalsIgnoreCase("ios")) {
            String output = runCommand("xcrun xctrace list devices");

            for (String line : output.split("\n")) {
                line = line.trim();
                if (!line.endsWith("(Simulator)") && line.startsWith("iPhone")) {
                    String[] udid = line.replace("[", "")
                            .replace("]", "")
                            .split(" ");
                    devices.add(udid[udid.length - 1]);
                }
            }
        } else {
            devices.addAll(ADB.getConnectedDeviceName());
        }
        return devices;
    }


    // Get list of the available simulators
    private static ArrayList<HashMap> getAvailableSimulatorsIOS() {

        HashMap<String, String> deviceInfo;

        ArrayList<HashMap> simulators = new ArrayList();

        String output = runCommand("instruments -s devices");

        for (String line : output.split("\n")) {
            line = line.trim();
            if (line.endsWith("(Simulator)") && line.startsWith("iPhone")) {
                String[] info = line.replace("[", "")
                        .replace("]", "")
                        .split(" ");

                String name = info[0] + " " + info[1];
                if (!info[2].contains("(")) {
                    name = name + " " + info[2];
                }
                String os = info[info.length - 3].replace("(", "").replace(")", "");
                deviceInfo = new HashMap<>();
                deviceInfo.put("name", name);
                deviceInfo.put("OS", os);
                simulators.add(deviceInfo);
            }
        }
        return simulators;
    }


    @Test
    public void runcomandTest() {
        System.out.println(getConnectedDevices("ios"));
    }

    @Test
    public void clearPOrtTest() {
        System.out.println(isDeviceConnected("iphone7_sim"));
    }


}

