package core.database;

import base.Base;
import core.Properties;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static core.Log.log;

public class DataBaseConnections  extends Base {


    // FULL LIST OF DBs
    //https://monese.atlassian.net/wiki/spaces/MO/pages/1426161679/Development

    final String TAG = "      DataBaseConnections():             | "; // 35 + |
    Properties up = new Properties();

    private static final DataBaseConnections instance = new DataBaseConnections();

    public static DataBaseConnections getInstance() {
        return instance;
    }

    private ThreadLocal<Connection> am_db_connection = new ThreadLocal<>();
    private ThreadLocal<Connection> core_db_connection = new ThreadLocal<>();


    public Connection connectAM() {
        Long startTime = System.currentTimeMillis();
        synchronized (this) {
            do {
                if (!isConnectionAlive(am_db_connection.get())) {
                    // log(TAG + "connectAM(): NEW connection");
                    try {
                /*
                Properties props = new Properties();
                props.setProperty("user", up.getUser());
                props.setProperty("password", up.getUser());
                props.setProperty("ssl", "true");
                am_db_connection.set(DriverManager.getConnection(up.getAmUrl(), props));
                // need install amazon cert -> https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/UsingWithRDS.SSL.html
                // other way add '?sslmode=require' to url -> https://jdbc.postgresql.org/documentation/head/ssl-client.html#nonvalidating
                */
                        am_db_connection.set(DriverManager.getConnection(
                                up.getAmUrl(),
                                up.getAMUserName(),
                                up.getAmPass()));
                    } catch (Exception e) {
                        log("FAILED\n");
                        e.printStackTrace();
                    }
                } else {
                    break;
                }
                sleep(200);
            } while (System.currentTimeMillis() < startTime + 10 * 1000); // 10 sec
        }
        return am_db_connection.get();
    }

    public Connection connectCore() {
        Long startTime = System.currentTimeMillis();
        do {
            if (!isConnectionAlive(core_db_connection.get())) {
                // log(TAG + "connectCore(): making NEW connection");
                try {
                    core_db_connection.set(DriverManager.getConnection(
                            up.getUrl(),
                            up.getCoreUserName(),
                            up.getCorePassword()));
                } catch (Exception e) {
                    log(TAG + "connectCore(): FAILED\n");
                    e.printStackTrace();
                }
            } else {
                break;
            }
            sleep(100);
        } while (System.currentTimeMillis() < startTime + 10 * 1000); // 10 sec
        return core_db_connection.get();
    }






    private boolean isConnectionAlive(Connection connection) {
        // log(TAG + "isConnectionAlive(): start");
        ThreadLocal<Boolean> isConnectionAlive = new ThreadLocal<>();
        if (connection == null) {
            isConnectionAlive.set(false);
        } else {
            try {
                isConnectionAlive.set(connection.isValid(1));
            } catch (SQLException ex) {
                log(TAG + "isConnectionAlive(): FAILED\n" + ex.getMessage());
                isConnectionAlive.set(false);
            }
        }
        // log(TAG + "isConnectionAlive(): DONE " + isConnectionAlive.get());
        return isConnectionAlive.get();

    }

    /**
     * @param connection - Connection to be closed
     */
    protected void closeConnection(Connection connection) {
        //  log(TAG + "closeConnection(): ");
        if (isConnectionAlive(connection)) {
            try {
                connection.close();
            } catch (SQLException e) {
                log(TAG + "closeConnection(): FAILED\n" + e.getMessage());
                e.printStackTrace();
            }
        }
    }

    /**
     * Closes all connections
     */
    public void closeAllConnections() {
        closeConnection(core_db_connection.get());
        closeConnection(am_db_connection.get());

    }
}
