package core;

import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;

public class ADB {

    private String id;

    public ADB(String deviceID) {

        this.id = deviceID;
    }

    public static String command(String command) {
        if (command.startsWith("adb")) {
            command = command.replace("adb ", ServerManager.getAndroidHome() + "/platform-tools/adb ");
        } else {
            throw new RuntimeException("This method is only to run ADB commands");
        }
        String output = ServerManager.runCommand(command);


        if (output == null || output.equals("") || output.equals(" ")) {
            return "";
        } else return output;
    }


    /*
    Kill Server Command
     */
    public static void killServer() {
        command("adb kill-server");
    }

    /*
   Start Server Command
    */
    public static void startServer() {
        command("adb start-server");
    }

    /*
    This method returns Array of names of Connected devices
     */
    public static ArrayList getConnectedDeviceName() {
        ArrayList devices = new ArrayList();
        String output = command("adb devices");

        for (String line : output.split("\n")) {
            line = line.trim();
            if (line.endsWith("device")) {
                devices.add(line.replace("device", "").trim());
            }
        }
        return devices;
    }


    /*
    This method returns name of foreground activity
     */
    public String getForegroundActivity() {
        return command("adb -s " + id + " shell dumpsys activity");
    }

    /*
    This method returns the Android version of  device attached as String
     */
    public String getAndroidVersionAsString() {
        String output = command("adb -s " + id + " shell getprop ro.build.version.release");
        if (output.length() == 3) {
            output += ".0";
        }

        return output;
    }

    /*
    This method returns Android version as Integer
     */
    public int getAndroidVersion() {
        return Integer.parseInt(getAndroidVersionAsString().replace("\\.", ""));
    }

    /*
    This method shows all packages installed on the device
     */

    public ArrayList getInstalledPackages() {
        ArrayList packages = new ArrayList();
        String[] output = command("adb -s " + id + " shell pm list packages").split("\n");
        for (String packageID : output) {
            packages.add(packageID.replace("package:", "").trim());
        }
        return packages;
    }

    /*
    This method clears app data
     */

    public void clearAppsData(String packageID) {
        command("adb -s " + id + " shell pm clear " + packageID);
    }

    /*
    This method force stops App
     */
    public void forceStopApp(String packageID) {
        command("adb -s " + id + " shell am force-stop " + packageID);
    }


    public void startApp(String packageID, String activityId) {
        command("adb -s " + id + " shell am start -n " + packageID + "/" + activityId);
    }

    /*
    Install app
     */
    public void installApp(String apkPath) {
        command("adb -s " + id + " install " + apkPath);
    }

    /*
    Uninstall app
     */
    public void uninstallApp(String packageId) {
        command("adb -s " + id + " uninstall" + packageId);
    }

    /*
    Clear Log Buffer
     */

    public void clearLogBuffer() {
        command("adb -s " + id + " shell logcat -c");
    }

    /*
    Push the file to the device
     */
    public void pushFile(String source, String target) {
        command("adb -s " + id + " push " + source + " " + target);
    }

    /*
    Pull the file from the device
     */

    public void pullFile(String source, String target) {
        command("adb -s " + id + " pull " + source + " " + target);
    }

    /*
    Delete the file
     */

    public void deleteFile(String target) {
        command("adb -s " + id + " shell rm" + target);
    }

    /*
    Move file from one folder to another on the device
     */

    public void moveFile(String source, String target) {
        command("adb -s " + id + " shell mv " + source + " " + target);
    }

    /*
    Take a screenshot
     */
    public void takeScreenshot(String target) {
        command("adb -s " + id + " shell screencap " + target);
    }


    /*
    Reboot device
     */
    public void rebootDevice() {
        command("adb -s " + id + " reboot");
    }

    /*
    Get device model
     */
    public String getDeviceModel() {
        return command("adb -s " + id + " shell getprop ro.product.model");
    }

    /*
    Get devices Serial Number
     */
    public String getSerialNumber() {
        return command("adb -s " + id + " shell getprop ro.serialno");
    }


    /*
    Get device carrier
     */
    public String getDeviceCarrier() {
        return command("adb -s " + id + " shell getprop gsm.operator.alpha");
    }


    /*
    Get List of LogCat processes
     */
    public ArrayList getLogcatProcesses() {
        String[] output = command("adb -s " + id + " shell top -n 1 | grep -i 'logcat'").split("\n");
        ArrayList processes = new ArrayList();
        for (String line : output) {
            processes.add(line.split(" ")[0]);
            processes.removeAll(Arrays.asList("", null));
        }
        return processes;
    }



    @Test
    public void getPackagesTest() {

        for (Object x : getInstalledPackages()) {
            System.out.println(x.toString());
        }

    }
}
