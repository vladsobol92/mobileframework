package core;

import base.Base;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.*;
import io.appium.java_client.touch.LongPressOptions;
import io.appium.java_client.touch.TapOptions;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.ElementOption;
import io.appium.java_client.touch.offset.PointOption;
import io.qameta.allure.Step;
import lombok.Getter;
import lombok.Setter;
import org.openqa.selenium.*;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.*;

import static io.appium.java_client.touch.WaitOptions.waitOptions;
import static java.time.Duration.ofMillis;
import static core.Log.*;

public abstract class MobileGestures extends Base {

    final String TAG = "      MobileGestures():              | "; // 35 + |

    @HowToUseLocators(iOSAutomation = LocatorGroupStrategy.ALL_POSSIBLE)
    @iOSXCUITFindBy(id = "OK")
    @iOSXCUITFindBy(id = "Allow")
    List <MobileElement> allowPermissions;

    @iOSXCUITFindBy(className = "XCUIElementTypeScrollView")
    @AndroidFindBy(className = "android.widget.ScrollView")
    private List<MobileElement> scrollView;


    protected AppiumDriver driver;
    @Getter
    @Setter
    String platform;

    public MobileGestures (AppiumDriver driver){

        this.driver=driver;
    }

    public boolean isDeviceIOS() {
        return driver.getCapabilities().getCapability("platformName")
                .toString()
                .equalsIgnoreCase("MAC");
    }

    public boolean isDeviceAndroid() {
        return driver.getCapabilities()
                .getCapability("platformName")
                .toString().equalsIgnoreCase("LINUX");
    }

    // Allow permissions (e.g. Location service, Contacts etc. )
    public void allowPermissionsAndroid() {
        log(TAG+"allowPermissionsAndroid()");
        driver.switchTo()
                .alert()
                .accept();
    }


    public void allowPermissionsIOS() {
        log(TAG+"allowPermissionsIOS()");
        try {
            tap(allowPermissions);
        } catch (Exception ex) {
            log(TAG+"allowPermissionsIOS(): FAILED");
        }

    }





    /*
    * Tap by Mobilelement using TapOptions
    * This is more "native" tap but has issue on iOS simulator (sometimes)
     */

    public boolean tap(MobileElement el, int num) {
        TapOptions tapOptions;
        boolean result = false;
        if (el == null)
            return result;

        for (int i = 0; i < num; i++) {
            for (int j = 0; j < 5; j++) { // 5 tries to make tap
                try {
                    tapOptions = new TapOptions().withElement(ElementOption.element(el));
                    new TouchAction(driver).tap(tapOptions).perform();
                    result = true;
                    break;
                } catch (StaleElementReferenceException e) {
                    log(TAG + "tap(): try " + j + " : FAILED with StaleElementReferenceException");
                } catch (Exception e) {
                    if (e.getMessage().contains("Could not proxy command to remote server")
                            || e.getMessage().contains("is not visible on the screen")) { // try it again
                        sleep(200);
                        log(TAG + "tap(): try " + j + " : FAILED");
                    } else {
                        log(TAG + "tap(): try 1: Element " + el + "\n" + e.getMessage());
                        break;
                    }
                }
            }
            if (!result)
                break;
            sleep(100);
        }
        sleep(200); // iOS too long animation
        return result;
    }


    public boolean longTap(MobileElement el, int num) {

        boolean result = false;
        if (el == null)
            return result;

        for (int i = 0; i < num; i++) {
            for (int j = 0; j < 5; j++) { // 5 tries to make tap
                try {
                    if (isDeviceAndroid()) {
                        LongPressOptions longPressOptions = new LongPressOptions().withElement(ElementOption.element(el));
                        new TouchAction(driver).longPress(longPressOptions).release().perform();
                    } else {
                        PointOption pointOption = new PointOption().withCoordinates(el.getCenter().x, el.getCenter().y);
                        new TouchAction(driver)
                                .press(pointOption)
                                .waitAction(WaitOptions.waitOptions(Duration.ofMillis(2000)))
                                .release().perform();
                    }
                    result = true;
                    break;
                } catch (Exception ex1) {
                    if (ex1.getMessage().contains("Could not proxy command to remote server") || ex1.getMessage().contains("is not visible on the screen")) { // try it again
                        sleep(200);
                        log(TAG + "longTap(): try " + j + " : FAILED");
                    } else {
                        log(TAG + "longTap(): try 1: Element " + el + "\n" + ex1.getMessage());
                        break;
                    }
                }
            }
            if (!result)
                break;
            sleep(100);
        }
        sleep(200); // iOS too long animation
        return result;
    }

    /*
    * Tap by List
    *
     */

    public boolean tap(List<MobileElement> elList) {
        try {
            return tap(elList.get(0), 1);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /*
    * iOS native tap
    * works on both simulator and device
     */
    public boolean tapElement_XCTest(MobileElement el) {
        try {
            // tap into center of element
            JavascriptExecutor js = (JavascriptExecutor) driver;
            HashMap<String, String> tapObject = new HashMap<String, String>();
            tapObject.put("x", String.valueOf(el.getSize().getWidth() / 2));
            tapObject.put("y", String.valueOf(el.getSize().getHeight() / 2));
            tapObject.put("element", el.getId());
            js.executeScript("mobile:tap", tapObject);

            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /*
    * Click MobilElement initialized by PageFactory
     */

    public boolean tap(MobileElement element){

       return tap(element,1);
    }


    /*
     * Click MobilElement by locator
     * First find the element and then click it
     */

    public void tap(By by){
        List<MobileElement> element =  driver.findElements(by);
        tap(element);
    }

    /*
    * Select vale from pickerWheel with ios driver
    * @valueToBeSelected is value until which driver will be scrolling picker wheel
     */

    public void setValueInPickerWheelIOS(String valueToBeSelected) {

        MobileElement countryPickerWheel= (MobileElement) driver.findElementByClassName("XCUIElementTypePickerWheel");
        String direction = valueToBeSelected.compareTo(countryPickerWheel.getText()) < 1 ? "previous" : "next";

        while (!countryPickerWheel.getText().contains(valueToBeSelected)) {
            HashMap<String, Object> params = new HashMap<>();
            params.put("order", direction);
            params.put("offset", 0.15);
            params.put("element", (countryPickerWheel).getId());
            driver.executeScript("mobile: selectPickerWheelValue", params);
        }
    }

    /*
     *Set value in picker wheel for Android
     */
    public void setValueInPickerWheelAndroid(MobileElement datePicker, String valueToBeSelected, String scrollDirection) {
        int startX = datePicker.getCenter().x;
        int startY= datePicker.getCenter().y;

        int finishY=scrollDirection.equalsIgnoreCase("down")?datePicker.findElements(By.className("android.widget.TextView"))
                .get(2).getCenter().y :datePicker.findElements(By.className("android.widget.TextView")).get(0).getCenter().y;

        PointOption start = new PointOption().withCoordinates(startX,startY);

        PointOption finish =new PointOption().withCoordinates(startX,finishY);

        TouchAction action=new TouchAction(driver);

        while (!datePicker.findElements(By.className("android.widget.TextView")).get(1).getText().contains(valueToBeSelected)){
            action.press(start).perform().moveTo(finish).release().perform();
        }

    }


    public boolean isElementPresent(List<MobileElement> listOfElements) {
        return !listOfElements.isEmpty();

    }

    public boolean isElementShown(By by) {
        log(TAG+"isElementShown(): "+by);
        List<MobileElement> listOfElements = driver.findElements(by);
        return !listOfElements.isEmpty();

    }

    /*
    * Hide keyboard after entering text into input field on ios device
     */
    private void hideKeyboardIOS(){
      log("Closing keyboard on iOS");
        try {
            driver.findElement(By.id("Return")).click();
        }
        catch (NoSuchElementException ex){
            log(TAG + "hideKeyboardIOS(): FAILED");
        }
    }

    /*
    * Wait until element @e is displayed for @seconds
     */
    public boolean waitUntil(MobileElement e,int seconds) {
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        return wait.until(webDriver -> e.isDisplayed());
    }


    public boolean waitUntil(By by, int seconds) {
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        return wait.until(webDriver -> driver.findElement(by).isDisplayed());
    }


    /*
     * Wait until element @e is clickable for @seconds
     */
    public void waitUntilClickable(MobileElement e,int seconds) {
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        wait.until(ExpectedConditions.elementToBeClickable(e));
    }

    public void waitUntilClickable(By by ,int seconds) {
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(by)));
    }


    /*
    * Enter text into input field
     */
    public void enterText(MobileElement e, String keys) {
        e.sendKeys(keys);
        if(isDeviceIOS()){
            hideKeyboardIOS();
        }
    }

    public void enterText(By by, String keys) {
        driver.findElement(by).sendKeys(keys);
        if(isDeviceIOS()){
            hideKeyboardIOS();
        }
    }


    public MobileElement scrollToElementInTableViewIOS(String elementsID) {

        waitUntil(By.className("XCUIElementTypeTable"),15);

        MobileElement parentId = (MobileElement) driver.findElementByClassName("XCUIElementTypeTable");

        MobileElement element=null;

        HashMap<String, String> scrollObject = new HashMap<String, String>();

        if(isElementShown(By.id(elementsID))) {
            element =(MobileElement) driver.findElementById(elementsID);
        }
        else {

            while (!isElementShown(By.id(elementsID))) {

                scrollObject.put("element", parentId.getId());
                scrollObject.put("direction", "down");
                driver.executeScript("mobile:scroll", scrollObject);  // scroll to the target element
            }
            element =(MobileElement) driver.findElementById(elementsID);
        }
        return element;
    }


    public MobileElement scrollToElementInScrollViewIOS(String elementsID) {

        waitUntil(By.className("XCUIElementTypeScrollView"),20);

        MobileElement parentId = (MobileElement) driver.findElementByClassName("XCUIElementTypeScrollView");

        HashMap<String, String> scrollObject = new HashMap<>();
        scrollObject.put("element", parentId.getId());
        // Use the predicate that provides the value of the label attribute
        scrollObject.put("name", elementsID);
        driver.executeScript("mobile:scroll", scrollObject);  // scroll to the target element

        return (MobileElement) driver.findElementByAccessibilityId(elementsID);
    }

    /*
    * Execute drag and drop. This method is helpfull for hablding sliders on IOS devices
    * @element - start point
    * @direction - direction to drag
    * @pixels - for how many pixels
     */
    public void dragScreenVerticalFromElementIOS(MobileElement element, String direction, int pixels){

        int elementX = element.getCenter().x;
        int elementY = element.getCenter().y;
        double finish;

        if(direction.equalsIgnoreCase("up")){
            finish = elementY-pixels;
        }

        else {
            finish = elementY+pixels;
        }

        JavascriptExecutor js = driver;
        HashMap<String, Object> params = new HashMap<>();
        params.put("duration", 2.0);
        params.put("fromX", elementX);
        params.put("fromY", elementY);
        params.put("toX", elementX);
        params.put("toY", finish);
        js.executeScript("mobile: dragFromToForDuration", params);

    }


    public void dragScreenHorizontalFromElementIOS(MobileElement element, String direction, int pixels){

        int elementX = element.getCenter().x;
        int elementY = element.getCenter().y;
        double finish;

        if(direction.equalsIgnoreCase("right")){
            finish = elementX+pixels;
        }

        else {
            finish = elementX-pixels;
        }

        JavascriptExecutor js = driver;
        HashMap<String, Object> params = new HashMap<>();
        params.put("duration", 2.0);
        params.put("fromX", elementX);
        params.put("fromY", elementY);
        params.put("toX", finish);
        params.put("toY", elementY);
        js.executeScript("mobile: dragFromToForDuration", params);

    }
    /*
    * @element - Element to start drag from
    * @direction - E.g. "left" or "right"
    * @pixels - for how many pixels
     */
    public void dragHorizontalAndroid(MobileElement element, String direction, int pixels){

        int elementX = element.getCenter().x;
        int elementY = element.getCenter().y;

        PointOption start = new PointOption().withCoordinates(elementX,elementY);

        PointOption finish;

        if(direction.equalsIgnoreCase("right")){
            finish = new PointOption().withCoordinates(elementX+pixels,elementY);
        }

        else {
            finish = new PointOption().withCoordinates(elementX-pixels,elementY);
        }

        TouchAction action=new TouchAction(driver);
        action.press(start).waitAction(WaitOptions.waitOptions(Duration.ofMillis(500))).moveTo(finish).perform();

    }

    /*
    * Different implementations of Android scrollToElement
     */

    public MobileElement scrollToElementAndroid(String elementText){
        return   (MobileElement) driver.findElement(MobileBy.AndroidUIAutomator(
                "new UiScrollable(new UiSelector().scrollable(true).instance(0))" +
                        ".scrollIntoView(new UiSelector()" +
                        ".textMatches(\"" + elementText + "\").instance(0))"));
    }

    public MobileElement scrollToElementContainsTextAndroid(String elementText){
        return   (MobileElement) driver.findElement(MobileBy.AndroidUIAutomator(
                "new UiScrollable(new UiSelector().scrollable(true).instance(0))" +
                        ".scrollIntoView(new UiSelector()" +
                        ".textContains(\"" + elementText + "\").instance(0))"));
    }


    public MobileElement scrollToElementByResourceIDAndroid(String resourceId){
        return   (MobileElement) driver.findElement(MobileBy.AndroidUIAutomator(
                "new UiScrollable(new UiSelector().scrollable(true).instance(0))" +
                        ".scrollIntoView(new UiSelector()" +
                        ".resourceId(\"" + resourceId + "\").instance(0))"));
    }

    public String UiAutomatorScrollableID(String resourceId){
        return "new UiScrollable(new UiSelector().scrollable(true).instance(0))" +
                ".scrollIntoView(new UiSelector()" +
                ".resourceIdMatches(\".*id/" + resourceId + "\"))";
    }

    /*
     * Different implementations of Android scrollToElement.
     * Is used with shortened resource id
     */
    public MobileElement scrollToElementByResourceIDMatchesAndroid(String resourceId) {
        log(TAG + "scrollToElementByResourceIDAndroid(): " + resourceId);
        return (MobileElement) driver.findElement(MobileBy.AndroidUIAutomator(
                "new UiScrollable(new UiSelector().scrollable(true).instance(0))" +
                        ".scrollIntoView(new UiSelector()" +
                        ".resourceIdMatches(\".*id/" + resourceId + "\"))"));
    }



    /**
     * Does scroll inside "android.widget.ScrollView" for Android
     * Does scroll inside "XCUIElementTypeScrollView" for iOS
     *
     * @dir : d - down, u - up
     */
    @Step("Scroll screen")
    public void scrollScreen(String dir) {
        log(TAG + "scrollScreen():");
        MobileElement el = scrollView.get(0);

        int startX = el.getCenter().x;
        int startY = el.getCenter().y;
        int endY = 0;

        if (dir.equals("d")) {
            endY = el.getLocation().y - 2;
        }
        if (dir.equals("u")) {
            endY = el.getCenter().y * 2;
        }

        new TouchAction(driver).press(PointOption.point(startX, startY))
                .waitAction(WaitOptions.waitOptions(Duration.ofMillis(200)))
                .moveTo(PointOption.point(startX, endY)).release().perform();

    }

    /*
     * Performs scrolling for the size of the half of visible area of the screen
     * * @dir : d - down, u - up
     */
    @Step("Scroll screen")
    public void scrollHalfOfTscreenHeight(String dir) {

        log(TAG + "scrollHalfOfTscreenHeight():");

        int screenHeight = driver.manage().window().getSize().height;
        int screenWidth = driver.manage().window().getSize().width;
        int startY = 0;
        int endY = 0;

        if (dir.equals("d")) {
            startY = screenHeight / 2;
            endY = 50;
        }
        if (dir.equals("u")) {
            startY = screenHeight / 2;
            endY = screenHeight - 50;
        }
        new TouchAction(driver).press(PointOption.point(screenWidth / 2, startY))
                .waitAction(WaitOptions.waitOptions(Duration.ofMillis(500)))
                .moveTo(PointOption.point(screenWidth / 2, endY))
                .release().perform();

    }


    /**
     * Does scroll inside XCUIElementTypeScrollView
     *
     * @param dir scroll direction. accepts "u", "d", "l", "r"
     * @return boolean
     */
    public boolean scrollTo_iOS(String dir) {
        log(TAG + "scrollTo_iOS(): dir " + dir);
        List<MobileElement> scrollView = driver.findElements(By.className("XCUIElementTypeScrollView"));
        if (!scrollView.isEmpty()) {
            return scrollViewTo_iOS(scrollView.get(0), dir);
        }
        return false;
    }

    /**
     * Does scroll inside given element
     *
     * @param el  element to scroll
     * @param dir scroll direction. accepts "u", "d", "l", "r"
     * @return boolean
     */
    public boolean scrollViewTo_iOS(MobileElement el, String dir) {
        log(TAG + "scrollViewTo_iOS(): dir " + dir);
        // The main difference from swipe call with the same argument is that scroll will try to move
        // the current viewport exactly to the next/previous page (the term "page" means the content,
        // which fits into a single device screen)
        try {
            JavascriptExecutor js = driver;
            HashMap<String, String> scrollObject = new HashMap<String, String>();
            if (dir.equals("d")) {
                scrollObject.put("direction", "up"); // scroll -> down, swipe -> up
            } else if (dir.equals("u")) {
                scrollObject.put("direction", "down"); // scroll -> up, swipe -> down
            } else if (dir.equals("l")) {
                scrollObject.put("direction", "left");
            } else if (dir.equals("r")) {
                scrollObject.put("direction", "right");
            }
            scrollObject.put("element", el.getId());
            scrollObject.put("toVisible", "true"); // optional but needed sometimes
            js.executeScript("mobile:swipe", scrollObject); // swipe faster the scroll
            sleep(300); // wait animation to complete
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void swipeUp(){

        TouchAction action=new TouchAction(driver);
        PointOption start=new PointOption().withCoordinates(300,1300);
        PointOption finish=new PointOption().withCoordinates(300,582);
        action.press(start).perform().waitAction(WaitOptions.waitOptions(Duration.ofSeconds(2))).moveTo(finish).release().perform();
    }

    public void swipeDown() {
        TouchAction action = new TouchAction(driver);
        PointOption start=new PointOption().withCoordinates(300,582);
        PointOption finish=new PointOption().withCoordinates(350,1300);
        action.press(start).perform().moveTo(finish).perform();
    }


    public void swipeUpByCoordinates(int StartX, int StartY, int FinishX, int FinishY){

        TouchAction action=new TouchAction(driver);
        PointOption start=new PointOption().withCoordinates(StartX,StartY);
        PointOption finish=new PointOption().withCoordinates(FinishX,FinishY);
        action.press(start).perform().moveTo(finish).release().perform();
    }


    public void enterAmountFromKeyboardViewAndroid(String value) {
        String[] valueNumbers = value.split("");
        String[] digits = {"zero_textview", "one_textview",
                "two_textview", "three_textview",
                "four_textview", "five_textview",
                "six_textview", "seven_textview",
                "eight_textview", "nine_textview"};
        waitUntil(By.id(digits[1]), 15);
        for (int i = 0; i < valueNumbers.length; i++) {
            int codeDigit = Integer.parseInt(valueNumbers[i]);
            waitUntil(By.id(digits[codeDigit]), 15);
            driver.findElement(By.id(digits[codeDigit])).click();
        }
    }

    public void enterAmountFromKeyboardViewIOS(String value){
        waitUntil(By.name("1"),15);
        String[] valueNumbers = value.split("");
        String[] digits = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
        for (int i = 0; i < valueNumbers.length; i++) {
            int codeDigit = Integer.parseInt(valueNumbers[i]);
            driver.findElement(By.name(digits[codeDigit])).click();
        }

    }

    public void sleep(int msec) {
        try {
            Thread.sleep(msec);
        } catch (Exception e) {
            // ignore
        }
    }

}
