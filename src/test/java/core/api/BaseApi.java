package core.api;

import base.Base;
import io.qameta.allure.Attachment;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.simple.JSONObject;

import static core.Log.log;

public class BaseApi extends Base{

    String exampleBaseUrl="http://example.com";

    protected RequestSpecification exampleRequestSpecification = new RequestSpecBuilder()
            .setBaseUri(exampleBaseUrl)
            .addHeader("Accept", "application/json")
            .addHeader("Content-type", "application/json")
            .build();


    public Response postRequest(RequestSpecification requestSpec, String url) {
        Response response = null;
        for (int i = 0; i < 2; i++) { // 2 times
            response = RestAssured
                    .given().spec(requestSpec)
                    //.log().all()
                    .when().post(url)
                    .then()
                    // .log().all()
                    .extract().response();

            try {
                if (response.getBody().asString().contains("502 Proxy Error")) {
                    sleep(1000);
                    log("502 ERROR!");
                    continue;
                }
            } catch (Exception e) {
                log("Error receiving Post request: " + e.getMessage());
                e.printStackTrace();
            }
            break;
        }
        return response;
    }


    protected Response putRequest(RequestSpecification requestSpec, String url) {
        Response response = RestAssured
                .given()
                .spec(requestSpec)
                // .log().all()
                .when().put(url)
                .then()
                // .log().all()
                .extract().response();
        return response;
    }

    protected Response getRequest(RequestSpecification requestSpec, String url) {
        Response response = RestAssured
                .given().spec(requestSpec)
                // .log().all()
                .when().get(url)
                .then()
                // .log().all()
                .extract().response();
        return response;
    }

    protected void logResponse(final Response response) {
        try {
            log("response: \n" + response.getBody().asString());
        } catch (Exception e) {
            log("response: log FAILED");
        }
    }

    public <T> T getResponsePath(Response response, String path) {
        T result = null;
        try {
            result = response.path(path);
        } catch (Exception ex) {
            log("FAILED to get '" + path + "' from '" + response.body().asString() + "'");
        }
        return result;
    }

    protected boolean isResponseStatusOK(Response response) {
        String result = getResponsePath(response, "status");
        return result != null && result.equals("OK");
    }

    // EXAMPLE

    public Response exampleRequest(){
        log("exapmleRequest(): ");

        JSONObject requestData = new JSONObject();
        requestData.put("param_1", 1);
        requestData.put("param_2", 2);
        attachParameters(requestData);

        RequestSpecification reqSpec = RestAssured.given()
                .spec(exampleRequestSpecification)
                .header("authorization", "SOME HEADER")
                .body(requestData);
        Response response = postRequest(reqSpec, "/endpoint");

        attachResponse(response);
        logResponse(response);

        return response;
    }

    /*
     *   This method attaches response body to Allure report
     */
    @Attachment(value = "responseBody", type = "text/json")
    public String attachResponse(Response response) {
        return response.getBody().asString();
    }


    /*
     *   This method attaches parameters of the request to Allure report
     */
    @Attachment(value = "parametersOfRequest", type = "text/json")
    protected String attachParameters(JSONObject parameters) {
        return parameters.toString();
    }

}
