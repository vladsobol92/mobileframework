package core;

import base.Base;

import java.io.FileInputStream;
import java.io.IOException;

public class Properties extends Base {

    java.io.InputStream is = getApplicationProperties();
    java.util.Properties p = new java.util.Properties();


    public synchronized java.io.InputStream getApplicationProperties() {
        String env = System.getProperty("testEnvironment").toLowerCase();

        String appPath = USER_HOME + "/Documents/someFolder/";
        String propertiesFile = env.contains("staging") ? (appPath + "staging_env.properties") : (appPath + "dev_env.properties");

        try {
            return new FileInputStream(propertiesFile);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }


    public String getCoreUserName() {
        try {
            p.load(is);
            return p.getProperty("core_username");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

    public String getCorePassword() {
        try {
            p.load(is);
            return p.getProperty("core_pass");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

    public String getAMUserName() {
        try {
            p.load(is);
            return p.getProperty("am_username");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

    public String getAmPass() {
        try {
            p.load(is);
            return p.getProperty("am_pass");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

    public String getAviosUser() {
        try {
            p.load(is);
            return p.getProperty("aviosUsername");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

    public String getAviosPassword() {
        try {
            p.load(is);
            return p.getProperty("aviosPassword");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getReccurUser() {
        try {
            p.load(is);
            return p.getProperty("reccurUsername");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

    public String getReccurPassword() {
        try {
            p.load(is);
            return p.getProperty("reccurPass");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getCardsDBusername() {
        try {
            p.load(is);
            return p.getProperty("cardsUsername");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

    public String getCardsDBPassword() {
        try {
            p.load(is);
            return p.getProperty("cardsPassword");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }


    public String getMerchantUser() {
        try {
            p.load(is);
            return p.getProperty("merchantUsername");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

    public String getMerchantPassword() {
        try {
            p.load(is);
            return p.getProperty("merchantPass");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }


    public String getPotsUsername() {
        try {
            p.load(is);
            return p.getProperty("savingPotsUsername");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

    public String getPotsPassword() {
        try {
            p.load(is);
            return p.getProperty("savingPotsPassword");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

    // Promotions

    public String getPromotionsUserName() {
        try {
            p.load(is);
            return p.getProperty("promotionsUsername");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

    public String getPromotionsPassword() {
        try {
            p.load(is);
            return p.getProperty("promotionsPassword");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

    public String getJAUserName() {
        try {
            p.load(is);
            return p.getProperty("jaUsername");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

    public String getJAPassword() {
        try {
            p.load(is);
            return p.getProperty("jaPassword");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

    public String getPort() {
        try {
            p.load(is);
            return p.getProperty("core.port");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

    public String getUrl() {
        try {
            p.load(is);
            return p.getProperty("ds.core.url");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

    public String getAviosUrl() {
        try {
            p.load(is);
            return p.getProperty("ds.avios.url");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getBusinessServiceUrl() {
        try {
            p.load(is);
            // TODO ask to stop this shit!
            return p.getProperty("business.service.url").replaceAll("business.dev.monese.com", "business.msp.dev.monese.com");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

    public String getAmUrl() {
        try {
            p.load(is);
            return p.getProperty("ds.am.url");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

    public String getReccurUrl() {
        try {
            p.load(is);
            return p.getProperty("ds.reccur.url");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

    public String getMerchantUrl() {
        try {
            p.load(is);
            return p.getProperty("ds.merchant.url");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

    public String getPotsUrl() {
        try {
            p.load(is);
            return p.getProperty("ds.savingpots.url");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

    public String getPromotionsUrl() {
        try {
            p.load(is);
            return p.getProperty("ds.promotions.url");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

    public String getBackOfficeUrl() {
        try {
            p.load(is);
            return p.getProperty("bo.url");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

    public String getCardsApiUrl() {
        try {
            p.load(is);
            return p.getProperty("cards.api.url");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

    public String getCardsDatabaseUrl() {
        try {
            p.load(is);
            return p.getProperty("ds.cards.url");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

    public String getDeviceDbUrl() {
        try {
            p.load(is);
            return p.getProperty("ds.device.url");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }


    public String getDeviceDbUsername() {
        try {
            p.load(is);
            return p.getProperty("deviceUsername");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }


    public String getDeviceDbPassword() {
        try {
            p.load(is);
            return p.getProperty("devicePassword");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getJAUrl() {
        try {
            p.load(is);
            return p.getProperty("ds.ja.url");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

    public String getCardsApiToken() {
        try {
            p.load(is);
            return p.getProperty("cards.api.token");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

    public String getBusinessApiToken() {
        try {
            p.load(is);
            return p.getProperty("business.api.token");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }


    public String getsPotsApiToken() {
        try {
            p.load(is);
            return p.getProperty("pots.api.token");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

    public String getPotsApiUrl() {
        try {
            p.load(is);
            return p.getProperty("pots.api.url");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

    public String getJAApiToken() {
        try {
            p.load(is);
            return p.getProperty("ja.api.token");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

    public String getJAApiUrl() {
        try {
            p.load(is);
            return p.getProperty("ja.api.url");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }


    public String getApiUrl() {
        try {
            p.load(is);
            return p.getProperty("api.url");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }


    public String getSettingsUrl() {
        try {
            p.load(is);
            return p.getProperty("ds.settings.url");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getSettingsUsername() {
        try {
            p.load(is);
            return p.getProperty("settingsUsername");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getSettingsPassword() {
        try {
            p.load(is);
            return p.getProperty("settingsPassword");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
