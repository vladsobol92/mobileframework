package core.testrail;

import core.testrail.testrail_api.APIClient;
import org.json.simple.JSONObject;
import org.testng.*;
import org.testng.annotations.ITestAnnotation;
import org.testng.annotations.Listeners;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class TestRailListener implements IAnnotationTransformer, ITestListener, IInvokedMethodListener {

    private final static APIClient client = new APIClient("https://vladyslav.testrail.io");

    public static String TestRunID = System.getProperty("testRun");

    public static APIClient getClient (){
            client.setUser("vladsobol@mail.ru");
            client.setPassword("fXd5Lttx/wcP8kgCGbd2");
        return client;
    };

    public String getTestCaseID(ITestResult result){
        String TestCaseID = null;
        IClass obj = result.getTestClass();
        Class newobj = obj.getRealClass();
        Method testMethod = null;
        try {
            testMethod = newobj.getMethod(result.getName());
            if (testMethod.isAnnotationPresent(TestRail.class)) {
                TestRail useAsTestName = testMethod.getAnnotation(TestRail.class);
                // Get the TestCase ID for TestRail
                TestCaseID = useAsTestName.testRailId().replaceAll("[^\\d]", "");
            }
        } catch (Exception ex){
            // ignore
        }

        return TestCaseID;
    }


    @Override
    public void transform(ITestAnnotation annotation, Class testClass, Constructor testConstructor, Method testMethod) {

    }

    @Override
    public void beforeInvocation(IInvokedMethod method, ITestResult testResult) {

    }

    @Override
    public void afterInvocation(IInvokedMethod method, ITestResult testResult) {

    }

    @Override
    public void onTestStart(ITestResult result) {

    }

    @Override
    public void onTestSuccess(ITestResult result) {
        String caseId = getTestCaseID(result);
        // Now you can call the TestRail post API call to update the result in TestRail database based on the TestRun Id and TestCase Id.
        Long executionTime = (result.getEndMillis() - result.getStartMillis())/1000;
        Map data = new HashMap();
        data.put("status_id",1); // 1 - pass/ 2 - Blocked / 4 - Retest / 5 -Failed
        data.put("comment", "The test passed\n"+"execution time: " + executionTime + " sec.");

        try {
            JSONObject r = (JSONObject) getClient().sendPost("add_result_for_case/"+TestRunID+"/"+caseId, data);
        } catch (Exception ex){
            ex.printStackTrace();
        }
    }


    @Override
    public void onTestFailure(ITestResult result) {

        String caseId = getTestCaseID(result);
        // Now you can call the TestRail post API call to update the result in TestRail database based on the TestRun Id and TestCase Id.
        Long executionTime = (result.getEndMillis() - result.getStartMillis())/1000;
        Map data = new HashMap();
        data.put("status_id",5); // 1 - pass/ 2 - Blocked / 4 - Retest / 5 -Failed
        data.put("comment", "The test FAILED\n" + result.getThrowable()  + " sec.");

        try {
            JSONObject r = (JSONObject) getClient().sendPost("add_result_for_case/"+TestRunID+"/"+caseId, data);
        } catch (Exception ex){
            ex.printStackTrace();
        }

    }

    @Override
    public void onTestSkipped(ITestResult result) {

    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {

    }

    @Override
    public void onStart(ITestContext context) {

    }

    @Override
    public void onFinish(ITestContext context) {

    }
}
