package core.recorder;

import core.ServerManager;

import static core.Log.*;
public class iOSSimulatorRecorder implements Runnable {


    final String TAG = "    iOSSimulatorRecorder():        | "; // 35 + |

  //  String videoFileName;
    String pathToSaveVideo;
    String udid;
    private Thread t;

    iOSSimulatorRecorder(String udid, String pathToSaveVideo){
     //   this.videoFileName = videoFileName;
        this.pathToSaveVideo = pathToSaveVideo;
        this.udid = udid;
    }

    @Override
    public void run() {
        //TODO replase to ServerManager.runcomand()
        //TODO Implement retry if failed to start recording
            try {
                Runtime.getRuntime().exec("src/test/resources/startRecording_ios" + " " + udid + " " + pathToSaveVideo).getInputStream();
            } catch (Exception ex) {
                //throw new RuntimeException(ex.getMessage());
                log(TAG + "run(): ERROR");
            }
    }

    // Start recording process in new thread
    public Thread startRecording(){
        t = new Thread(this);
        t.start();
        return t;
    }

    public void stopRecording(Thread t){
        log(TAG+"stopRecording()");
        String processId = getRecordingProcessId();
        int retry=0;
        do {
            ServerManager.runCommand("kill -INT " +processId); // kill recording process id
            retry++;
        } while (!ServerManager.getRecordingProcesses(udid).isEmpty() && retry < 3); // try to kill process 3 times
        retry = 0;
        do {
            log(TAG+"stopRecording() interrupt thread");
            try {
                t.interrupt();
            }catch (Exception ex){
                log(TAG+"stopRecording(): error occurred while stopping IOS Simulator recording");
            }
            retry++;
        }
        while (t.isAlive() && retry < 3);
    }

    // get ID of recording process ps -ax
    private String getRecordingProcessId(){
        log(TAG+"getRecordingProcessId()");
        String processId = ServerManager.getRecordingProcesses(udid).get(0).toString();;
        return processId;
    }

}
