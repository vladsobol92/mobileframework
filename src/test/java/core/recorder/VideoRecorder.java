package core.recorder;


import com.google.common.io.Files;
import core.ServerManager;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSStartScreenRecordingOptions;
import io.qameta.allure.Attachment;
import io.qameta.allure.Step;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.testng.ITestResult;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Base64;

import static core.Log.log;

public class VideoRecorder {

    final String TAG = "    VideoRecorder():                | "; // 35 + |

    @Getter @Setter
    String platform;


    @Getter @Setter
    String udid;

    @Getter @Setter
    String videoFileLocation;

    @Getter @Setter
    String device;

    @Getter @Setter
    Thread t;

    AppiumDriver driver;

    iOSSimulatorRecorder simulatorRecorder;

    String methodName;

   private final static String ANDROID_VIDEO_FOLDER = System.getProperty("user.home")+"/Desktop/video/android/";;
   private final static String IOS_VIDEO_FOLDER = System.getProperty("user.home")+"/Desktop/video/ios/";


   public VideoRecorder (String methodNAme, String device, AppiumDriver driver){
       this.methodName = methodNAme;
       this.device = device;
       this.driver = driver;
   }

    public void startRecording(String platform, String udid){

        log("  startRecording(): "+platform);
        setPlatform(platform);
        setUdid(udid);

        if (platform.equalsIgnoreCase("ios")) {

            if (device.contains("sim")) {
                setVideoFileLocation(IOS_VIDEO_FOLDER + "simulator_"+methodName+"_"+ System.currentTimeMillis()+".mp4");
                simulatorRecorder = new iOSSimulatorRecorder(udid,getVideoFileLocation());
                t = simulatorRecorder.startRecording();

            }
            else {
           // ServerManager.runCommand("flick video -a start -p " + platform.toLowerCase() + " -u " + udid + " -e true");
                ((IOSDriver)driver).startRecordingScreen(new IOSStartScreenRecordingOptions()
                                .withFps(5)
                                .withVideoScale("320:-2")
                                .withVideoType("h264")); // mpeg4 / h264)
            System.out.println("Video Record started ");
            }
        }

        else {
            startAndroidScreenRecord(udid);
        }

    }

    public void stopRecording(String videoFileName, ITestResult result){
        log("  stopRecording(): "+videoFileName);
        if(getPlatform().equalsIgnoreCase("ios")) {

            if (device.contains("sim")){  // for simulator
                simulatorRecorder.stopRecording(t);
            }
            else { // for real device
           //     ServerManager.runCommand("flick video -a stop -p ios -u " + udid + " -o " + IOS_VIDEO_FOLDER + " -n "
           //             + videoFileName + " -f mp4");


                setVideoFileLocation( (IOS_VIDEO_FOLDER + videoFileName + ".mp4" ));

                String resultRecord = ((IOSDriver) driver).stopRecordingScreen();
                byte[] decoded = Base64.getDecoder().decode(resultRecord);
                try {
                    FileUtils.writeByteArrayToFile(new File(getVideoFileLocation()), decoded);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                log(TAG+"stopRecording(): Video File saved to: " + getVideoFileLocation());
            }
        } else {
            setVideoFileLocation(videoFileName);
            stopAndroidRecord(getUdid());
          //  if(!result.isSuccess()){ // if result = SUCCESS then do not pull video
                pullVideoFromDevice(getUdid(), getVideoFileLocation());
        //    }
            deleteVideoFromDevice(getUdid());
        }

    }


    public void deleteVideo(){
        log("  deleteVideo():");
        if (getPlatform().equalsIgnoreCase("ios")) {

            File file=null;
            try {
                file = new File(getVideoFileLocation().replace(":", "_"));
            }
            catch (Exception ex){
                // ignore
                log(TAG+"deleteVideo(): FAILED");
            }
            if (file.delete()) {
                log(TAG+"deleteVideo(): SUCCESS" + file.toString());
            } else {
                log(TAG+"deleteVideo(): NO file to delete" + file.toString());
            }
        }

        else {
            try {
                File videoFolder = new File(ANDROID_VIDEO_FOLDER +getVideoFileLocation());
                FileUtils.deleteDirectory(videoFolder);
            } catch (IOException e) {
               //ignore
            }
        }

    }


    public void startAndroidScreenRecord(String deviceName){
        log("  startAndroidScreenRecord()");
        AndroidRecorder androidRecorder = new AndroidRecorder(deviceName);
        t = new Thread(androidRecorder);
        t.start();
        ServerManager.getRecordingProcesses(deviceName);

    }


    public void stopAndroidRecord(String deviceName){
        log("  stopAndroidRecord: " +deviceName);

        for (Object processId : ServerManager.getRecordingProcesses(deviceName) ) {
           String command = "kill "+processId.toString();
           ServerManager.runCommand(command);
           t.interrupt();

      }
    }

    public void pullVideoFromDevice(String deviceName, String destination){
        log("  pullVideoFromDevice:() "+deviceName +" "+destination);

       // String path = "log/android-video/";
       // String path = ANDROID_VIDEO_FOLDER;
        new File(ANDROID_VIDEO_FOLDER+destination).mkdirs(); // create subfolder with method name inside ANDROID_VIDEO_FOLDER

        String command = "adb -s " +deviceName+
                " pull /sdcard/testPart1.mp4 "+ ANDROID_VIDEO_FOLDER +destination;

        String command2="adb -s " +deviceName+
                " pull /sdcard/testPart2.mp4 "+ ANDROID_VIDEO_FOLDER +destination;

        String command3="adb -s " +deviceName+
                " pull /sdcard/testPart3.mp4 "+ ANDROID_VIDEO_FOLDER +destination;
        ArrayList recordingProcess= ServerManager.getRecordingProcesses(deviceName);

        while (recordingProcess.size()>0){
           // stopRecording(deviceName);
            stopAndroidRecord(deviceName);
            recordingProcess= ServerManager.getRecordingProcesses(deviceName);
        }

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ServerManager.runCommand(command);
        ServerManager.runCommand(command2);
        ServerManager.runCommand(command3);
    }


    private void deleteVideoFromDevice(String deviceName){
        String command1 = "adb -s " + deviceName+ " shell rm /sdcard/testPart1.mp4";
        String command2 = "adb -s " + deviceName+ " shell rm /sdcard/testPart2.mp4";
        String command3 = "adb -s " + deviceName+ " shell rm /sdcard/testPart3.mp4";
        ServerManager.runCommand(command1);
        ServerManager.runCommand(command2);
        ServerManager.runCommand(command3);
    }

    @Step("Video")
    public void attachVideo(){
        log("  attachVideo():");
        if(getPlatform().equalsIgnoreCase("ios")) {
            String location = getVideoFileLocation().replace(":","_");
            if (device.contains("sim")){
                attachVideoToAllure(getVideoFileLocation());
            }
            else {
                attachVideoToAllure(location);
            }
        } else {
            attachVideoToAllure(ANDROID_VIDEO_FOLDER + getVideoFileLocation()+"/testPart1.mp4");
            attachVideoToAllure(ANDROID_VIDEO_FOLDER +getVideoFileLocation()+"/testPart2.mp4");
            attachVideoToAllure(ANDROID_VIDEO_FOLDER +getVideoFileLocation()+"/testPart3.mp4");
        }
        log("  attachVideo(): DONE");
    }

    /*
    * Attach video file to allure reporter
     */
    @Attachment(value = "VideoFile", type = "video/mp4", fileExtension = ".mp4")
    private byte[]attachVideoToAllure(String videoFileLocation){

        log("  attachVideoToAllure(): ");
        log("  attachVideoToAllure(): " + videoFileLocation);
        File video = new File(videoFileLocation);
        try {
            return Files.toByteArray(video);
        } catch (IOException e) {
            //e.printStackTrace();
            log("  attachVideoToAllure(): FAILED");
            return new byte[0];
        }

    }

}
