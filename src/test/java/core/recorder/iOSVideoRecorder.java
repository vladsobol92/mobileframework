package core.recorder;

import base.Base;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSStartScreenRecordingOptions;
import io.qameta.allure.Step;
import lombok.Getter;
import org.apache.commons.io.FileUtils;
import org.testng.ITestResult;

import static core.Log.log;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.Base64;

public class iOSVideoRecorder extends Base implements Runnable{

    final String TAG = "      iOSRecorder():             | "; // 35 + |

    private AppiumDriver driver;

    @Getter
    private String recordingResult;


    iOSVideoRecorder(AppiumDriver driver) {
        this.driver = driver;
    }

    @Override
    public void run() {
        startIOsRecording();
    }

    private void startIOsRecording() {
        log(TAG + "startIOsRecording()");
        long startTime = System.currentTimeMillis();
        try {
            ((IOSDriver) driver).startRecordingScreen(
                    new IOSStartScreenRecordingOptions()
                            .withFps(30)
                            .withVideoScale("320:-2")
                            .withVideoType("h264") // mpeg4 / h264
                            .withTimeLimit(Duration.ofMinutes(8))); // 8 minutes max. video lenght

            Long sec = (System.currentTimeMillis() - startTime) / 1000;
            log(TAG + "startIOsRecording(): DONE " + sec);
        } catch (Exception ex) {
            log(TAG + "startIOsRecording(): FAILED");
        }


    }

    public void stopRecording() {
        if (driver == null) {
            log(TAG + "stopRecording(): FAILED. Driver NULL");
            return;
        }

        Long startTime = System.currentTimeMillis();
        do {
            try {
                recordingResult = ((IOSDriver) driver).stopRecordingScreen();
                break;
            } catch (Exception ex) {
                log(TAG + "stopRecording(): FAILED. Stop recording correctly!");
                ex.printStackTrace();
            }
            sleep(1000);
        } while (System.currentTimeMillis() < startTime + 5 * 1000); // 5 sec

        if (recordingResult == null || recordingResult.equals("")) {
            log(TAG + "stopRecording(): FAILED. Result is NULL!");
        }
    }

}
