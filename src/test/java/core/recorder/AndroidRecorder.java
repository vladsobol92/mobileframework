package core.recorder;

import core.ServerManager;

import java.io.IOException;

public class AndroidRecorder implements Runnable {

    String deviceName;

    AndroidRecorder(String deviceName){
        this.deviceName=deviceName;
    }

    @Override
    public void run() {
        ServerManager.runCommand("src/test/resources/startRecording" + " "+ deviceName);
    }
}
