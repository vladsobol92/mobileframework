package core;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;
import static core.Log.*;

public class Retry implements IRetryAnalyzer {
    private static int retryCount = 0;
    private int maxRetryCount = 1;   // retry a failed test 1 additional time

    public static void resetRetryCount() {
        retryCount = 0;
    }

    public static int getRetryCount() {
        return retryCount;
    }

    public boolean retry(ITestResult result) {
        // Test Statuses
        /*
        int CREATED = -1;
        int SUCCESS = 1;
        int FAILURE = 2;
        int SKIP = 3;
        int SUCCESS_PERCENTAGE_FAILURE = 4;
        int STARTED = 16;
        */
        log(" --------------- Retry count is: " + (retryCount + 1) + ", result - " + result.getStatus());


        if (String.valueOf(result.getAttribute("skipRetry")) != "true") {
            log(" --------------- Retry count is: " + (retryCount + 1) + ", result - " + result.getStatus());
            if (retryCount < maxRetryCount) {
                //result.getTestContext().getSkippedTests().removeResult(result.getMethod());
                retryCount++;
                return true;
            }
        }
        retryCount = 0;

        return false;
    }
}
