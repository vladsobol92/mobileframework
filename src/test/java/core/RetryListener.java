package core;

import org.testng.*;
import org.testng.annotations.ITestAnnotation;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Iterator;


public class RetryListener implements IAnnotationTransformer, ITestListener, IInvokedMethodListener {
    public void transform(ITestAnnotation annotation, Class testClass,
                          Constructor testConstructor, Method testMethod) {
        IRetryAnalyzer retry = annotation.getRetryAnalyzer();
        if (retry == null) {
            annotation.setRetryAnalyzer(Retry.class);
        }
    }


    @Override
    public void onTestStart(ITestResult iTestResult) {

    }

    @Override
    public void onTestSuccess(ITestResult iTestResult) {

    }

    @Override
    public void onTestFailure(ITestResult iTestResult) {

    }

    @Override
    public void onTestSkipped(ITestResult iTestResult) {

    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {
    }

    @Override
    public void onStart(ITestContext iTestContext) {

    }

    @Override
    public void onFinish(ITestContext iTestContext) {
        System.err.println("   RetryListener(): onFinish():");

        // check with FAILED
        Iterator<ITestResult> listOfFailedTests = iTestContext.getFailedTests().getAllResults().iterator();
        while (listOfFailedTests.hasNext()) {
            ITestResult failedTest = listOfFailedTests.next();
            ITestNGMethod method = failedTest.getMethod();
            // remove duplicated FAIL tests
            if (iTestContext.getFailedTests().getResults(method).size() > 1) {
                System.err.println("   RetryListener(): onFinish(): REMOVE DOUBLE FAIL");
                listOfFailedTests.remove();
            }
            // remove FAIL when retry PASS
            if (iTestContext.getPassedTests().getResults(method).size() > 0) {
                System.err.println("   RetryListener(): onFinish(): REMOVE FAIL when we have PASS");
                listOfFailedTests.remove();
            }
        }

        // check with SKIPPED
        Iterator<ITestResult> listOfSkippedTests = iTestContext.getSkippedTests().getAllResults().iterator();
        while (listOfSkippedTests.hasNext()) {
            ITestResult skippedTest = listOfSkippedTests.next();
            ITestNGMethod method = skippedTest.getMethod();
            // remove SKIPPED when we have FAIL
            if (iTestContext.getFailedTests().getResults(method).size() > 0) {
                System.err.println("   RetryListener(): onFinish(): REMOVE SKIPPED when we have FAIL");
                listOfSkippedTests.remove();
            }
        }
    }

    @Override
    public void beforeInvocation(IInvokedMethod iInvokedMethod, ITestResult iTestResult) {

    }

    @Override
    public void afterInvocation(IInvokedMethod method, ITestResult testResult) {

    }
}