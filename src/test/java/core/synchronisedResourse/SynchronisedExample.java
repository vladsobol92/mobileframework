package core.synchronisedResourse;

import edu.emory.mathcs.backport.java.util.Arrays;
import org.testng.Assert;

import java.util.LinkedList;
import java.util.List;
import static core.Log.log;

public class SynchronisedExample {

    final String TAG = "    SynchronisedExample():          | "; // 35 + |

    final int MAX_TIMEOUT = 300000; // 5 minutes

    // currently available phone numbers in Tallinn office
    private static String[] phoneNumbers_android = {"1", "2", "3", "4"};
    private static String[] phoneNumbers_ios = {"1", "2", "3"};
    // phone numbers available for taking during test
    private static List<String> availablePhonesList_android;
    private static List<String> availablePhonesList_ios;

    private static List<String> availablePhonesList;

    static final SynchronisedExample instance = new SynchronisedExample();

    /**
     * Takes one of the phone numbers available
     *
     * @return
     */

    public static SynchronisedExample getInstance() {
        return instance;
    }


    public void initValidPhonesList(String devicePlatform) {
        log(TAG + "initValidPhonesList(): ");
        // android
        if (devicePlatform.toLowerCase().contains("android")) {
            log(TAG + "initValidPhonesList(): Android...");
            if (availablePhonesList_android == null) {
                availablePhonesList_android = new LinkedList<>(Arrays.asList(phoneNumbers_android));
            }
            availablePhonesList = availablePhonesList_android;
            log(TAG + "initValidPhonesList(): Android Done: " + availablePhonesList.size() + " phones");
        }
        // ios
        else {
            if (availablePhonesList_ios == null) {
                availablePhonesList_ios = new LinkedList<>(Arrays.asList(phoneNumbers_ios));
            }
            availablePhonesList = availablePhonesList_ios;
            log(TAG + "initValidPhonesList(): iOS Done: " + availablePhonesList.size() + " phones");
        }
    }

    public String takePhoneNumber() {
        synchronized (availablePhonesList) {
            while (availablePhonesList.isEmpty()) {
                log(TAG + "takePhoneNumber(): Waiting to take phoneNumber...");
                try {
                    // will wait and wake up Thread automatically either after MAX_TIMEOUT time OR when availablePhonesList is NOT empty
                    availablePhonesList.wait(MAX_TIMEOUT);
                } catch (InterruptedException e) {
                    // ignore
                }
            }
            log(TAG + "takePhoneNumber(): Taking phone...");
            String phone;
            Assert.assertTrue(!availablePhonesList.isEmpty(),
                    TAG + "takePhoneNumber(): Failed to take phone from list: All phone numbers busy ");
            phone = availablePhonesList.get(0); // take first phone from availablePhonesList
            availablePhonesList.removeIf(s -> s.equals(phone));
            log(TAG + "takePhoneNumber(): Phone taken " + phone);

            availablePhonesList.notify(); // notify next waiting thread

            return phone;
        }

    }

    /**
     * Returns taken phone number to the list as we have limited amount of phone numbers for ForgotPassCode tests
     *
     * @param phoneNumber - number to be returned to the 'availablePhonesList'
     */
    public void returnPhoneNumber(String phoneNumber) {
        log(" returnPhoneNumber(): ");
        synchronized (availablePhonesList) {
            log(TAG + "returnPhoneNumber(): returning phone:  " + phoneNumber + " to the list...");
            availablePhonesList.add(phoneNumber);
            log(TAG + "returnPhoneNumber(): returning phone:  " + phoneNumber + " to the list: DONE");
            availablePhonesList.notify();
        }

    }

}
