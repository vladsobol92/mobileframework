package core.utils.logging;


import static core.Log.getCurrentTime;

public class Logger {

    public static synchronized <T> void log(T... messages) {
        System.out.println(constructLogMessage(true, true, messages));
    }

    public static synchronized <T> void logError(T... messages) {
        System.err.println(constructLogMessage(true, true, messages));
    }

    public static synchronized void logHorizontalLine() {
        StringBuilder builder = new StringBuilder("[" + getCurrentTime() + "]");
        builder.append(LoggerHelper.getFormattedCurrentThreadName());
        builder.append(" ------------------------------------------------");
        System.out.println(builder.toString());
    }

    public static synchronized String createAssertionLog(String message) {
        return constructLogMessage(true, false, message);
    }

    public static synchronized String assertionFailedMessage() {
        return createAssertionLog("FAILED");
    }

    // Helper methods
    private static <T> String constructLogMessage(boolean useIndentation, boolean detailed, T... messages) {
        StringBuilder builder = new StringBuilder();
        if (detailed) {
            builder.append(getCurrentTime());
            builder.append(LoggerHelper.getFormattedCurrentThreadName());
            builder.append(" ");
        }
        builder.append(LoggerHelper.getClassTag(useIndentation));
        // builder.append(" ");
        builder.append(LoggerHelper.getMethodTag().replace("| ", "").trim());
        builder.append(" ");
        for (T message : messages) {
            builder.append(message);
            builder.append(" ");
        }
        return builder.toString();
    }
}
