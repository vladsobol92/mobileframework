package core.utils.logging;

import base.BaseScreen;
import base.BaseTest;

import java.util.Arrays;
import java.util.List;

public class LoggerHelper {

    public static String getCurrentTime() {
        StringBuilder builder = new StringBuilder("[");
        builder.append(String.format("%12s", java.time.LocalTime.now()));
        builder.append("]");
        return builder.toString();
    }

    public static String getFormattedCurrentThreadName() {
        StringBuilder builder = new StringBuilder(" [");
        builder.append(Thread.currentThread().getName());
        builder.append("]");
        return builder.toString();
    }

    public static String getClassTag(boolean useIndentation) {
        try {
            Class callingClass = Class.forName(Thread.currentThread().getStackTrace()[4].getClassName());
            return getClassTag(callingClass, useIndentation);
        } catch (ClassNotFoundException e) {
            System.err.println(e.getMessage());
            return null;
        }
    }

    public static String getClassTagByPage(BaseScreen page) {
        return getClassTag(page.getClass(), true);
    }

    private static String getClassTag(Class callingClass, boolean useIndentation) {
        String tagName = callingClass.getSimpleName() + ":";
        if (!useIndentation) {
            return tagName;
        }
        int indent = 2; // high level like beforeMethod, suite and testrail
        List<String> midLevelPaths = Arrays.asList("core.utils", "customers");
        if (callingClass == BaseScreen.class) {
            indent = 6;
        } else if (isSubclassOf(callingClass, BaseScreen.class) ||
                isDirectSubclassOf(callingClass, BaseTest.class) ||
                midLevelPaths.parallelStream().anyMatch(callingClass.getCanonicalName()::contains)) {
            indent = 4;
        }
        return getTag(tagName, indent);
    }

    public static String getMethodTag() {
        // you don't want the name of this method, but the calling method.
        StackTraceElement[] stackTrace = new Exception().getStackTrace();
        for (int i = 0; i < stackTrace.length; i++) {
            StackTraceElement stackTraceElement = new Exception().getStackTrace()[i];
            String className = stackTraceElement.getClassName();
            String methodName = stackTraceElement.getMethodName();
            if (!className.contains("core.utils.logging") && !methodName.contains("invoke")) {
                int indent = 6;
                if (className.equals("base.Base") || className.equals("base.BaseTest")) {
                    indent = 2;
                }
                return getTag(methodName + "():", indent);
            }
        }
        return getTag("UNKNOWN():", 6);
    }

    private static String getTag(String name, int prefixIndent) {
        // example: "      getAccounts():            | "
        // 32 chars
        StringBuilder builder = new StringBuilder();
        builder.append(String.format("%1$" + prefixIndent + "s", ""));
        int suffixIndent = 32 - prefixIndent;
        builder.append(String.format("%-" + suffixIndent + "s| ", name));
        return builder.toString();
    }

    private static boolean isDirectSubclassOf(Class baseClass, Class superClass) {
        if (baseClass.getSuperclass() == null) {
            return false;
        }
        return baseClass.getSuperclass() == superClass;
    }

    private static boolean isSubclassOf(Class baseClass, Class superClass) {
        if (baseClass.equals(superClass)) {
            return true;
        } else {
            baseClass = baseClass.getSuperclass();
            if (baseClass.equals(Object.class)) {
                return false;
            }
            return isSubclassOf(baseClass, superClass);
        }
    }
}
