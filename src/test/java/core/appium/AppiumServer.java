package core.appium;

import base.Base;
import core.Device;
import core.ServerManager;
import core.utils.logging.Logger;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.AndroidServerFlag;
import io.appium.java_client.service.local.flags.GeneralServerFlag;
import io.qameta.allure.Step;
import lombok.Getter;
import org.testng.Assert;

import java.io.File;
import java.util.concurrent.TimeUnit;


public class AppiumServer extends Base {
    private final static String APPIUM_SERVICE_IP = "127.0.0.1";

    @Getter
    private AppiumDriverLocalService service;

    @Getter
    private Device device;

    public AppiumServer(Device device) {
        this.device = device;
    }

    /**
     * THis method initiates Service Builder to start Appium Server with specific arguments
     *
     * @param appiumServerPort
     * @return
     */
    @Step("Appium server: get service builder")
    private AppiumServiceBuilder getServiceBuilder(String appiumServerPort, boolean saveLogs) {
        Logger.log(String.format("port: %s, saveLogs: %s", appiumServerPort, saveLogs));
        AppiumPorts ports = new AppiumPorts(appiumServerPort);

        // Clear ports
        ServerManager.clearPort(ports.appium);
        ServerManager.clearPort(ports.bootstrap);
        ServerManager.clearPort(ports.chromePort);

        AppiumServiceBuilder appiumServiceBuilder = new AppiumServiceBuilder()
                .withIPAddress(APPIUM_SERVICE_IP)
                .usingPort(ports.appium)
                .withArgument(GeneralServerFlag.SESSION_OVERRIDE)
                .withStartUpTimeOut(180, TimeUnit.SECONDS) // 3 min
                .withArgument(AndroidServerFlag.BOOTSTRAP_PORT_NUMBER, String.valueOf(ports.bootstrap))
                .withArgument(AndroidServerFlag.CHROME_DRIVER_PORT, String.valueOf(ports.bootstrap))
                //   .withArgument(GeneralServerFlag.TEMP_DIRECTORY, tmpFolder)
                .withArgument(GeneralServerFlag.LOG_TIMESTAMP)
                .withArgument(GeneralServerFlag.LOG_LEVEL, "error");
        if (saveLogs) {
            File logFile = new File("./target/" + System.currentTimeMillis() + ".log");
            appiumServiceBuilder.withLogFile(logFile);
        }


        Logger.log("DONE");
        return appiumServiceBuilder;

    }

    @Step("Appium server: start server")
    public AppiumServer startServer(boolean saveLogs) {
        Logger.log();
        AppiumServiceBuilder builder = getServiceBuilder(device.getAppiumPort(), saveLogs);

        // build service
        service = AppiumDriverLocalService
                .buildService(builder);
        // start
        service.start();
        // check is running
        boolean serviceIsRunning = false;
        int attempt = 0;
        while (attempt <= 3) {
            attempt++;
            serviceIsRunning = service.isRunning();
            if (serviceIsRunning) {
                break;
            } else {
                Logger.log("Waiting for Appium Server to be start...");
                sleep(2000);
            }
        }
        Assert.assertTrue(serviceIsRunning, Logger.createAssertionLog("AppiumManager: FAILED to start Appium Server on port " + device.getAppiumPort()));
        Logger.log("DONE");
        return this;
    }

    @Step("Appium server: stop server")
    public void stopServer() {
        Logger.log();
        service.stop();
        Logger.log("DONE");

    }


}
