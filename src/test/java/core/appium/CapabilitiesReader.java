package core.appium;

import base.Base;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class CapabilitiesReader extends Base {

    public static JSONObject readCapabilitiesFile() {

        JSONParser parser = new JSONParser();

        Object obj = null;

        try {
            obj = parser.parse(new FileReader("capabilities.json"));
        } catch (FileNotFoundException noFile) {
            noFile.printStackTrace();
        } catch (IOException io) {
            io.printStackTrace();
        } catch (ParseException parserEx) {
            parserEx.printStackTrace();
        }

        JSONObject fileData = (JSONObject) obj;

        return fileData;
    }
}
