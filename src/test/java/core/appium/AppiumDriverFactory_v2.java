package core.appium;

import base.Base;
import core.Device;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.Setting;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.remote.*;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.qameta.allure.Step;
import org.apache.http.util.TextUtils;
import org.json.simple.JSONObject;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.PageFactory;

import java.io.File;
import java.time.Duration;
import java.util.*;

import static core.Device.DevicePlatform;
import static core.Log.log;

public class AppiumDriverFactory_v2 extends Base {
    private final static String TAG = "AppiumDriverFactory_v2:              |";

    @Step("Configure appium driver {devicePlatform},{testEnvironment},{testBuild},{deviceName},{processArguments}")
    private AppiumDriver configureAppiumDriver(Device device, String testEnvironment, String testBuild, String appPath, String processArguments, AppiumDriverLocalService appiumService) {
        log(String.format(TAG + "configureLocalAppium(): deviceName: %s,  devicePlatform: %s, startType: %s, testEnvironment:%s, testBuild: %s, appPath: %s, processArguments: %s ",
                device.getDeviceName(), device.getDevicePlatform(), device.getStartType(), testEnvironment, testBuild, appPath, processArguments));
        // https://github.com/appium/appium/blob/master/docs/en/advanced-concepts/settings.md

        String path = appPath;
        DesiredCapabilities capabilities = new DesiredCapabilities();
        final AppiumDriver[] driver = new AppiumDriver[1];

        // get device data
        AppiumPorts ports = new AppiumPorts(device.getAppiumPort());

        if (device.getDevicePlatform().equals(DevicePlatform.IOS)) {
            if (device.getDeviceName().contains("sim")) {
                path = path.replace(".ipa", ".app");
            }
            File appDir;
            appDir = new File(path);
            capabilities.setCapability(MobileCapabilityType.APP, appDir.getAbsolutePath());
            if (!device.getDeviceName().contains("sim")) {
                JSONObject iOSCapabilities = (JSONObject) CapabilitiesReader.readCapabilitiesFile().get("ios");
                capabilities.setCapability(MobileCapabilityType.UDID, device.getUdid());
                capabilities.setCapability(IOSMobileCapabilityType.XCODE_ORG_ID, iOSCapabilities.get("xcodeOrgId"));
                capabilities.setCapability(IOSMobileCapabilityType.XCODE_SIGNING_ID, iOSCapabilities.get("xcodeSigningId"));
            }
            capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, AutomationName.IOS_XCUI_TEST);

            if (testEnvironment.equals("dev"))
                capabilities.setCapability(IOSMobileCapabilityType.BUNDLE_ID, "com.monese.dev");
                // capabilities.setCapability(IOSMobileCapabilityType.BUNDLE_ID, "com.monese.WKWebViewDemo");
            else if (testEnvironment.equals("staging")) {
                if (testBuild.contains("release"))
                    capabilities.setCapability(IOSMobileCapabilityType.BUNDLE_ID, "com.monese.staging");
                else
                    capabilities.setCapability(IOSMobileCapabilityType.BUNDLE_ID, "com.monese.dev"); // we user dev with Flag
            }

            // capabilities.setCapability(IOSMobileCapabilityType.AUTO_ACCEPT_ALERTS, true);
            // capabilities.setCapability(IOSMobileCapabilityType.SIMPLE_ISVISIBLE_CHECK, false);
            capabilities.setCapability(IOSMobileCapabilityType.SHOULD_USE_SINGLETON_TESTMANAGER, false);
            capabilities.setCapability(IOSMobileCapabilityType.NATIVE_WEB_TAP, true);
            capabilities.setCapability(IOSMobileCapabilityType.SEND_KEY_STRATEGY, "grouped");
            capabilities.setCapability(IOSMobileCapabilityType.SHOW_XCODE_LOG, false);

            // speedup a bit with set of derivedDataPath
            capabilities.setCapability("derivedDataPath", USER_HOME + "/Library/Developer/Xcode/DerivedData/WebDriverAgent-ciegwgvxzxdrqthilmrmczmqvrgu");
            capabilities.setCapability(IOSMobileCapabilityType.WDA_STARTUP_RETRIES, 4); // default 2
            capabilities.setCapability(IOSMobileCapabilityType.IOS_INSTALL_PAUSE, 500); // default 0 ms
            //capabilities.setCapability(MobileCapabilityType.LANGUAGE, "ee");
            //capabilities.setCapability(MobileCapabilityType.LOCALE, "ee_EE"); // en_EE
            capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, device.getDeviceName());
            //  capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, "");
            capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, MobilePlatform.IOS);
            capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, device.getPlatformVersion());
            capabilities.setCapability("shouldTerminateApp", "true");
            capabilities.setCapability("forceAppLaunch", "true");
            // capabilities.setCapability(IOSMobileCapabilityType.SCALE_FACTOR, "0.5");
            capabilities.setCapability(IOSMobileCapabilityType.WDA_LOCAL_PORT, ports.wdaPort);
            //capabilities.setCapability(IOSMobileCapabilityType.WDA_CONNECTION_TIMEOUT, "60000"); //ms
            capabilities.setCapability(IOSMobileCapabilityType.WDA_LAUNCH_TIMEOUT, "120000");
            capabilities.setCapability("launchWithIDB", true); // TODO latest xcode may not work https://github.com/facebook/idb
            // capabilities.setCapability("additionalWebviewBundleIds", "['process-SafariViewService']");
            // capabilities.setCapability("additionalWebviewBundleIds", "process-SafariViewService");
            // capabilities.setCapability("safariLogAllCommunication", true);
            // capabilities.setCapability("safariLogAllCommunicationHexDump", true);
            capabilities.setCapability("fullContextList", true); // https://appiumpro.com/editions/61-how-to-accurately-select-webviews-using-the-fullcontextlist-capability
            capabilities.setCapability("useJSONSource", true);
            capabilities.setCapability("waitForQuiescence", "false"); // give a try ? default "true"
            // capabilities.setCapability("preventWDAAttachments", "false");
            // capabilities.setCapability(IOSMobileCapabilityType.START_IWDP, "true");
            // capabilities.setCapability(IOSMobileCapabilityType.ALLOW_TOUCHID_ENROLL, "true");
            capabilities.setCapability("mjpegServerPort", ports.video);
            // send arguments to switch app flags
            // default arguments: disables yellow warning on memory leak, enable generate IDs
            String arguments = "-featureFlag.CB: Disable leak alerts,YES,-featureFlag.DP: Hide phone contacts,NO,GENERATE_IDENTIFIERS,YES";
            if (processArguments != null && !processArguments.isEmpty()) {
                arguments = arguments.concat(",").concat(processArguments);
            }
            final List<String> processArgs = new ArrayList<>(Arrays.asList(arguments.split(",")));
            // final List<String> processArgs = new ArrayList<>(Arrays.asList("-featureFlag.paymentFlowTypeVariantA", "YES"));
            final Map<String, Object> processEnv = new HashMap<>();
            processEnv.put("key", "value");
            final JSONObject argsValue = new JSONObject();
            argsValue.put("args", processArgs);
            argsValue.put("env", processEnv);
            capabilities.setCapability(IOSMobileCapabilityType.PROCESS_ARGUMENTS, argsValue.toString());
            log(TAG + "configureAppiumDriver(): argsValue is: " + argsValue.toString());

        } else if (device.getDevicePlatform().equals(DevicePlatform.ANDROID)) {
            // capabilities = DesiredCapabilities.android();
            //AutomationName.APPIUM AutomationName.ANDROID_UIAUTOMATOR2 "espresso"
            capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, AutomationName.ANDROID_UIAUTOMATOR2);
            //capabilities.setCapability(AndroidMobileCapabilityType.UNICODE_KEYBOARD, "true");
            //capabilities.setCapability("unlockType", "pin");
            //capabilities.setCapability("unlockKey", "1234");

            if (device.getDeviceName() == null || TextUtils.isEmpty(device.getDeviceName())) { // connect to any device
                capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "Android");
            } else { // connect to specific device
                capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, device.getDeviceName());
                capabilities.setCapability(MobileCapabilityType.UDID, device.getUdid());
            }
            capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, MobilePlatform.ANDROID);
            capabilities.setCapability(AndroidMobileCapabilityType.AUTO_GRANT_PERMISSIONS, true);
            capabilities.setCapability(AndroidMobileCapabilityType.DISABLE_ANDROID_WATCHERS, true);
            capabilities.setCapability(AndroidMobileCapabilityType.SYSTEM_PORT, ports.system);
            capabilities.setCapability(AndroidMobileCapabilityType.NO_SIGN, "true");
            // 'enableMultiWindows' setting for UIA2 driver to get more items in the page source
            capabilities.setCapability("fullContextList", true);
            /*
            capabilities.setCapability(AndroidMobileCapabilityType.DONT_STOP_APP_ON_RESET, "true");
            */
            // get packageName and activity
            File appDir;
            appDir = new File(path);
            capabilities.setCapability(MobileCapabilityType.APP, appDir.getAbsolutePath());
            capabilities.setCapability(AndroidMobileCapabilityType.APP_WAIT_ACTIVITY, "com.monese.monese.*");
            // Feature flags
            if (processArguments != null && !processArguments.isEmpty()) {
                final String optionalIntentArguments = getAndroidFlags(processArguments);
                capabilities.setCapability(AndroidMobileCapabilityType.OPTIONAL_INTENT_ARGUMENTS, optionalIntentArguments);
            }

        }

        capabilities.setCapability(MobileCapabilityType.CLEAR_SYSTEM_FILES, true);
        capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 180);

        if (device.getStartType().equalsIgnoreCase("fullReset")) { // reinstall client
            log(TAG + "configureAppiumDriver(): Driver DO FULL-RESET");
            capabilities.setCapability(MobileCapabilityType.FULL_RESET, true);
            capabilities.setCapability(MobileCapabilityType.NO_RESET, false);

            // IOS
            capabilities.setCapability(IOSMobileCapabilityType.USE_NEW_WDA, true);
            capabilities.setCapability(IOSMobileCapabilityType.USE_PREBUILT_WDA, true);
        } else if (device.getStartType().equalsIgnoreCase("fastReset")) { // clears cache without reinstall
            log(TAG + "configureAppiumDriver(): Driver DO FAST-RESET");
            capabilities.setCapability(MobileCapabilityType.FULL_RESET, false);
            capabilities.setCapability(MobileCapabilityType.NO_RESET, false);
            // IOS
            capabilities.setCapability(IOSMobileCapabilityType.USE_NEW_WDA, false);
            capabilities.setCapability(IOSMobileCapabilityType.USE_PREBUILT_WDA, true);
        } else {
            log(TAG + "configureAppiumDriver(): Driver DO NORMAL start");
            capabilities.setCapability(MobileCapabilityType.FULL_RESET, false);
            capabilities.setCapability(MobileCapabilityType.NO_RESET, true);
            // ANDROID
            // speeds up session startup for cases where Appium doesn't need to wait for the device, push a settings app, or set permissions
            capabilities.setCapability("skipDeviceInitialization", true);
            // skips the step of installing the UIAutomator2 server
            capabilities.setCapability("skipServerInstallation", true);
            // IOS
            capabilities.setCapability(IOSMobileCapabilityType.USE_NEW_WDA, false);
            capabilities.setCapability(IOSMobileCapabilityType.USE_PREBUILT_WDA, true);
        }
        log(capabilities);

        if (device.getDevicePlatform().equals(DevicePlatform.ANDROID)) {
            log(TAG + "configureAppiumDriver(): AndroidDriver: " + appiumService.getUrl());
            driver[0] = new AndroidDriver<>(appiumService, capabilities);
            // https://appium.io/docs/en/advanced-concepts/settings/
            ((AndroidDriver) driver[0]).setSetting(Setting.WAIT_FOR_IDLE_TIMEOUT, 500); // TODO check if helps on screen where we have looong response?
        } else {
            log(TAG + "configureAppiumDriver(): IOSDriver: " + appiumService.getUrl());
            driver[0] = new IOSDriver<>(appiumService, capabilities);
            log(TAG + "configureAppiumDriver(): IOSDriver: STARTED");

        }

        return driver[0];

    }

    public AppiumDriver startDriver(Device device, String testEnvironment, String testBuild, String appPath, String
            processArguments, AppiumDriverLocalService appiumService) {
        log(TAG + "startDriver(): appPath - " + appPath);
        long startDriverTime = System.currentTimeMillis();
        log(TAG + "startDriver(): devicePlatform: " + device.getDevicePlatform() + ", deviceName: " + device.getDeviceName());

        // Start driver
        AppiumDriver driver = configureAppiumDriver(device, testEnvironment, testBuild, appPath, processArguments, appiumService);

        if (driver == null) {
            log(TAG + "startDriver(): FAILED to start!");
            return null;
        }
        try {
            PageFactory.initElements(new AppiumFieldDecorator(driver, Duration.ofSeconds(10)), this);
        } catch (Exception e) {
            log(TAG + "startDriver(): FAILED init timeouts!");
            e.printStackTrace();
        }

        startDriverTime = System.currentTimeMillis() - startDriverTime;
        log(TAG + "startDriver(): completed in - " + (int) startDriverTime / 1000 + " sec");
        return driver;
    }


    // CRD_VIRTUAL_CARDS,CRDT_ADD_SALARY:true,false  - pass arguments like this
    private String getAndroidFlags(String processArguments) {
        log(TAG + "getAndroidFlags() " + processArguments);
        // e.g. String to pass to capabilities as OPTIONAL_INTENT_ARGUMENTS
        // "--esa FEATURE_FLAG_KEYS CRD_VIRTUAL_CARDS,CRDT_ADD_SALARY --esa FEATURE_FLAG_VALUES true,true"

        String arguments = null;
        // parse values
        if (processArguments != null && !processArguments.isEmpty()) {
            // get Flag names
            LinkedList<String> flags = new LinkedList<>(Arrays.asList(processArguments.split(":")[0].split(",")));
            // get Flag values : true/false
            LinkedList<String> values = new LinkedList<>(Arrays.asList(processArguments.split(":")[1].split(",")));
            // prepare String to pass to capabilities
            arguments = "--esa FEATURE_FLAG_KEYS " + String.join(",", flags) + " --esa FEATURE_FLAG_VALUES" + " " + String.join(",", values);
        } else {
            log(TAG + "getAndroidFlags(): 'processArguments' - null, flags NOT set");
        }

        return arguments;
    }
}
