package core.appium;

import base.Base;
import base.BaseScreen;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.qameta.allure.Attachment;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import static core.Log.log;

public class PerformanceManager<T extends BaseScreen> extends Base {

    final String TAG = "    PerformanceManager():        | "; // 35 + |

    //private String PACKAGE_NAME;
    private AppiumDriver driver;
    private int pullTime;


    public List<HashMap<String, Object>> memoryInformation = new ArrayList<>();
    public List<JSONObject> cpuInformation = new ArrayList<>();

    public PerformanceManager(AppiumDriver driver, int pullTime) {
        this.driver = driver;
        this.pullTime = pullTime;
    }


    /**
     * @param packageName - app name e.g  com.monese.dev
     * @param type        - cpuinfo, batteryinfo, networkinfo, memoryinfo
     * @param interval    - time to pull data millisec.
     * @return
     */
    public HashMap<String, Double> getPerformanceData(String packageName, PerformanceType type, int interval) {
        log(TAG + "getPerformanceData(): " + type + " " + interval);
        HashMap<String, Double> readableData = new HashMap<>();
        List<List<Object>> data = ((AndroidDriver) driver).getPerformanceData(packageName, type.name().toLowerCase(), interval);
        for (int i = 0; i < data.get(0).size(); i++) {
            Double val;
            if (data.get(1).get(i) == null) {
                val = 0.0;
            } else {
                val = Double.parseDouble((String) data.get(1).get(i));
            }
            readableData.put((String) data.get(0).get(i), val);
        }
        return readableData;
    }

    /**
     * @param pageObject     - page object e.g. moneseApp().homePage()
     * @param memoryInfoType - MemoryInfoType e.g. nativePss, totalPss
     * @return value in kBs
     */
    @SuppressWarnings("unchecked")
    public Double getMemorySnapShot(T pageObject, MemoryInfoType memoryInfoType) {

        String packageName = driver.getCapabilities().getCapability("appPackage").toString();

        log(TAG + "getMemorySnapShot(): " + pageObject.getTAG() + " " + memoryInfoType);
        String screenName = pageObject.getTAG();
        HashMap<String, Object> memoryInfo = new HashMap<>();

        Double value = getPerformanceData(packageName, PerformanceType.MEMORYINFO, pullTime).get(memoryInfoType.name());
        if (value != null) {
            memoryInfo.put(screenName, value);
            memoryInformation.add(memoryInfo);
            return value;
        } else {
            memoryInfo.put(screenName, -1.0);
            memoryInformation.add(memoryInfo);
            return -1.0;
        }
    }


    /**
     * @param page - page object e.g. moneseApp().homePage()
     * @return
     */
    public JSONObject getCpuSnapShot(T page) {
        log(TAG + "getCpuSnapShot(): " + page.getTAG());

        String packageName = driver.getCapabilities().getCapability("appPackage").toString();

        HashMap<String, Double> performanceData = getPerformanceData(packageName, PerformanceType.CPUINFO, pullTime);
        Double userValue = performanceData.get(CPUInfoType.user.name());
        Double kernelValue = performanceData.get(CPUInfoType.kernel.name());

        String screenName = page.getTAG(); // name of the screen where we take snapshot
        JSONObject cpuData = new JSONObject(); // JSON object of cpu data: user, kernel
        JSONObject dataObject = new JSONObject(); // JSON object that will be added to 'cpuInformation' List

        // user
        if (userValue != null) {
            cpuData.put("user", userValue);
        } else {
            cpuData.put("user", -1);
        }
        // kernel
        if (kernelValue != null) {
            cpuData.put("kernel", kernelValue);

        } else {
            cpuData.put("kernel", -1);
        }
        dataObject.put(screenName, cpuData);
        cpuInformation.add(dataObject);

        return dataObject;
    }

    @Attachment(type = "text/plain")
    public String logMemoryInformation() {
        log(TAG + "logMemoryInformation(): ");
        String attachableResult = "";
        if (!memoryInformation.isEmpty()) {
            for (Object obj : memoryInformation) {
                log(obj.toString() + " kB");
                attachableResult = attachableResult.concat(obj.toString() + " kB" + "\n");
            }
        } else {
            log(TAG + "memoryInformation is NOT collected. 'memoryInformation.size() == 0");
        }
        return attachableResult;
    }

    @Attachment(type = "text/plain")
    public String logCPUInformation() {
        log(TAG + "logCPUInformation(): ");
        String attachableResult = "";
        if (!cpuInformation.isEmpty()) {
            for (Object obj : cpuInformation) {
                log(obj.toString());
                attachableResult = attachableResult.concat(obj.toString() + "\n");
            }
        } else {
            log(TAG + "CPU information is NOT collected 'cpuInformation.size() == 0'");
        }
        return attachableResult;
    }


    public enum PerformanceType {
        CPUINFO, BATTERYINFO, NETWORKINFO, MEMORYINFO
    }

    public enum MemoryInfoType {
        totalPrivateDirty, nativePrivateDirty,
        dalvikPrivateDirty, eglPrivateDirty, glPrivateDirty,
        totalPss, nativePss, dalvikPss, eglPss,
        glPss, nativeHeapAllocatedSize, nativeHeapSize
    }

    public enum CPUInfoType {
        user, kernel
    }
}
