package core.appium;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.Setting;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.remote.*;
import io.qameta.allure.Step;
import org.apache.http.util.TextUtils;
import org.json.simple.JSONObject;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.PageFactory;

import java.io.File;
import java.net.URL;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.*;

import static core.Log.log;

public class AppiumDriverFactory extends AppiumManager {

    public AppiumDriverFactory(String device) {
        super(device);
    }

    @Step("Configure local appium {devicePlatform},{testEnvironment},{testBuild},{deviceName},{processArguments}")
    private AppiumDriver configureAppiumDriver(String devicePlatform, String testEnvironment, String testBuild, String appPath, String deviceName, String processArguments) {
        log(TAG + "configureLocalAppium(): devicePlatform: " + devicePlatform + ", testEnvironment: " + testEnvironment + ", testBuild: " + testBuild + ", deviceName: " + deviceName);
        // https://github.com/appium/appium/blob/master/docs/en/advanced-concepts/settings.md

        String path = appPath;
        DesiredCapabilities capabilities = new DesiredCapabilities();
        final AppiumDriver[] driver = new AppiumDriver[1];

        // get device data
        final JSONObject[] androidCapabilities = {(JSONObject) readCapabilitiesFile().get("android")};
        JSONObject iOSCapabilities = (JSONObject) readCapabilitiesFile().get("ios");
        JSONObject deviceCapabilities = (JSONObject) readCapabilitiesFile().get(deviceName);
        AppiumDriverPorts ports = new AppiumDriverPorts(deviceCapabilities.get("appiumPort").toString());

        if (devicePlatform.contains("ios")) {
            // if (deviceName.contains("iphone")) capabilities = DesiredCapabilities.iphone();
            // else if (deviceName.contains("iPad")) capabilities = DesiredCapabilities.ipad();
            // else System.err.println("    configureLocalAppium(): DEVICE NAME for iOS platform is WRONG: " + deviceName);

            if (deviceName.contains("sim")) {
                path = path.replace(".ipa", ".app");
            }

            File appDir;
            appDir = new File(path);

            capabilities.setCapability(MobileCapabilityType.APP, appDir.getAbsolutePath());
            if (!deviceName.contains("sim")) {
                capabilities.setCapability(MobileCapabilityType.UDID, deviceCapabilities.get("udid"));
                capabilities.setCapability(IOSMobileCapabilityType.XCODE_ORG_ID, iOSCapabilities.get("xcodeOrgId"));
                capabilities.setCapability(IOSMobileCapabilityType.XCODE_SIGNING_ID, iOSCapabilities.get("xcodeSigningId"));
            }
            capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, AutomationName.IOS_XCUI_TEST);


            if (testEnvironment.equals("dev"))
                capabilities.setCapability(IOSMobileCapabilityType.BUNDLE_ID, "com.monese.dev");
                // capabilities.setCapability(IOSMobileCapabilityType.BUNDLE_ID, "com.monese.WKWebViewDemo");
            else if (testEnvironment.equals("staging")) {
                if (testBuild.contains("release"))
                    capabilities.setCapability(IOSMobileCapabilityType.BUNDLE_ID, "com.monese.staging");
                else
                    capabilities.setCapability(IOSMobileCapabilityType.BUNDLE_ID, "com.monese.dev"); // we user dev with Flag
            }

            // capabilities.setCapability(IOSMobileCapabilityType.AUTO_ACCEPT_ALERTS, true);
            // capabilities.setCapability(IOSMobileCapabilityType.SIMPLE_ISVISIBLE_CHECK, false);
            capabilities.setCapability(IOSMobileCapabilityType.SHOULD_USE_SINGLETON_TESTMANAGER, false);
            capabilities.setCapability(IOSMobileCapabilityType.NATIVE_WEB_TAP, true);
            capabilities.setCapability(IOSMobileCapabilityType.SEND_KEY_STRATEGY, "grouped");
            capabilities.setCapability(IOSMobileCapabilityType.SHOW_XCODE_LOG, false);
            // TODO derivedDataPath
            /*
            String derivedDataPath = System.getProperty("user.home") + "/Desktop/iOS_DerivedData/" + deviceName;
            Logger.log("derivedDataPath: '" + derivedDataPath + "'");
            capabilities.setCapability("derivedDataPath", derivedDataPath);
            */
            // speedup a bit with set of derivedDataPath
            capabilities.setCapability("derivedDataPath", System.getProperty("user.home") + "/Library/Developer/Xcode/DerivedData/WebDriverAgent-ciegwgvxzxdrqthilmrmczmqvrgu");
            capabilities.setCapability(IOSMobileCapabilityType.WDA_STARTUP_RETRIES, 4); // default 2
            capabilities.setCapability(IOSMobileCapabilityType.IOS_INSTALL_PAUSE, 500); // default 0 ms
            //capabilities.setCapability(MobileCapabilityType.LANGUAGE, "ee");
            //capabilities.setCapability(MobileCapabilityType.LOCALE, "ee_EE"); // en_EE
            capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, deviceCapabilities.get("deviceName"));
            //  capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, "");
            capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, MobilePlatform.IOS);
            capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, deviceCapabilities.get("platformVersion"));
            capabilities.setCapability("shouldTerminateApp", "true");
            capabilities.setCapability("forceAppLaunch", "true");
            // capabilities.setCapability(IOSMobileCapabilityType.SCALE_FACTOR, "0.5");
            capabilities.setCapability(IOSMobileCapabilityType.WDA_LOCAL_PORT, ports.wda);
            //capabilities.setCapability(IOSMobileCapabilityType.WDA_CONNECTION_TIMEOUT, "60000"); //ms
            capabilities.setCapability(IOSMobileCapabilityType.WDA_LAUNCH_TIMEOUT, "120000");
            capabilities.setCapability("launchWithIDB", true); // TODO latest xcode may not work https://github.com/facebook/idb
            // capabilities.setCapability("additionalWebviewBundleIds", "['process-SafariViewService']");
            // capabilities.setCapability("additionalWebviewBundleIds", "process-SafariViewService");
            // capabilities.setCapability("safariLogAllCommunication", true);
            // capabilities.setCapability("safariLogAllCommunicationHexDump", true);
            capabilities.setCapability("fullContextList", true); // https://appiumpro.com/editions/61-how-to-accurately-select-webviews-using-the-fullcontextlist-capability
            capabilities.setCapability("useJSONSource", true);
            capabilities.setCapability("waitForQuiescence", "false"); // give a try ? default "true"
            // capabilities.setCapability("preventWDAAttachments", "false");
            // capabilities.setCapability(IOSMobileCapabilityType.START_IWDP, "true");
            // capabilities.setCapability(IOSMobileCapabilityType.ALLOW_TOUCHID_ENROLL, "true");
            capabilities.setCapability("mjpegServerPort", ports.video);
            // send arguments to switch app flags
            // default arguments: disables yellow warning on memory leak, enable generate IDs
            String arguments = "-featureFlag.CB: Disable leak alerts,YES,-featureFlag.DP: Hide phone contacts,NO,GENERATE_IDENTIFIERS,YES";
            if (processArguments != null && !processArguments.isEmpty()) {
                arguments = arguments.concat(",").concat(processArguments);
            }
            final List<String> processArgs = new ArrayList<>(Arrays.asList(arguments.split(",")));
            // final List<String> processArgs = new ArrayList<>(Arrays.asList("-featureFlag.paymentFlowTypeVariantA", "YES"));
            final Map<String, Object> processEnv = new HashMap<>();
            processEnv.put("key", "value");
            final JSONObject argsValue = new JSONObject();
            argsValue.put("args", processArgs);
            argsValue.put("env", processEnv);
            capabilities.setCapability(IOSMobileCapabilityType.PROCESS_ARGUMENTS, argsValue.toString());
            log(TAG + "configureLocalAppium(): argsValue is: " + argsValue.toString());

        } else if (devicePlatform.contains("android")) {
            // capabilities = DesiredCapabilities.android();
            //AutomationName.APPIUM AutomationName.ANDROID_UIAUTOMATOR2 "espresso"
            capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, AutomationName.ANDROID_UIAUTOMATOR2);
            //capabilities.setCapability(AndroidMobileCapabilityType.UNICODE_KEYBOARD, "true");
            //capabilities.setCapability("unlockType", "pin");
            //capabilities.setCapability("unlockKey", "1234");

            if (deviceName == null || TextUtils.isEmpty(deviceName)) { // connect to any device
                capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "Android");
            } else { // connect to specific device
                capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, deviceName);
                capabilities.setCapability(MobileCapabilityType.UDID, deviceCapabilities.get("udid"));
            }
            capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, MobilePlatform.ANDROID);
            capabilities.setCapability(AndroidMobileCapabilityType.AUTO_GRANT_PERMISSIONS, true);
            capabilities.setCapability(AndroidMobileCapabilityType.DISABLE_ANDROID_WATCHERS, true);
            capabilities.setCapability(AndroidMobileCapabilityType.SYSTEM_PORT, ports.system);
            capabilities.setCapability(AndroidMobileCapabilityType.NO_SIGN, "true");
            // 'enableMultiWindows' setting for UIA2 driver to get more items in the page source
            capabilities.setCapability("fullContextList", true);
            /*
            capabilities.setCapability(AndroidMobileCapabilityType.DONT_STOP_APP_ON_RESET, "true");
            */
            // get packageName and activity
            File appDir;
            appDir = new File(path);
            capabilities.setCapability(MobileCapabilityType.APP, appDir.getAbsolutePath());
            /*
            if (testEnvironment.equals("dev"))
                capabilities.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "com.monese.monese.dev");
            else if (testEnvironment.equals("staging"))
                capabilities.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "com.monese.monese.staging");
            capabilities.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, "com.monese.monese.activities.LauncherActivity");
             */
            capabilities.setCapability(AndroidMobileCapabilityType.APP_WAIT_ACTIVITY, "com.monese.monese.*");
            // Feature flags
            if (processArguments != null && !processArguments.isEmpty()) {
                final String optionalIntentArguments = getAndroidFlags(processArguments);
                capabilities.setCapability(AndroidMobileCapabilityType.OPTIONAL_INTENT_ARGUMENTS, optionalIntentArguments);
            }

        }

        capabilities.setCapability(MobileCapabilityType.CLEAR_SYSTEM_FILES, true);
        capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 180);

        if (devicePlatform.contains("fullReset".toLowerCase())) { // reinstall client
            log(TAG + "configureLocalAppium(): Driver DO FULL-RESET");
            capabilities.setCapability(MobileCapabilityType.FULL_RESET, true);
            capabilities.setCapability(MobileCapabilityType.NO_RESET, false);
            capabilities.setCapability(IOSMobileCapabilityType.USE_NEW_WDA, true);
            // IOS
            // capabilities.setCapability(IOSMobileCapabilityType.USE_PREBUILT_WDA, false);
        } else if (devicePlatform.contains("fastReset".toLowerCase())) { // clears cache without reinstall
            log(TAG + "configureLocalAppium(): Driver DO FAST-RESET");
            capabilities.setCapability(MobileCapabilityType.FULL_RESET, false);
            capabilities.setCapability(MobileCapabilityType.NO_RESET, false);
            // IOS
            capabilities.setCapability(IOSMobileCapabilityType.USE_PREBUILT_WDA, true);
        } else {
            log(TAG + "configureLocalAppium(): Driver DO NORMAL start");
            capabilities.setCapability(MobileCapabilityType.FULL_RESET, false);
            capabilities.setCapability(MobileCapabilityType.NO_RESET, true);
            // ANDROID
            // speeds up session startup for cases where Appium doesn't need to wait for the device, push a settings app, or set permissions
            capabilities.setCapability("skipDeviceInitialization", true);
            // skips the step of installing the UIAutomator2 server
            capabilities.setCapability("skipServerInstallation", true);
            // IOS
            capabilities.setCapability(IOSMobileCapabilityType.USE_PREBUILT_WDA, true);
        }
        log(capabilities);

        String baseURL = "http://0.0.0.0:";
        String minorURL = "/wd/hub";
        String port = String.valueOf(ports.appium);

        final ExecutorService executor = Executors.newSingleThreadExecutor();
        if (devicePlatform.contains("android")) {
            log(TAG + "configureLocalAppium(): AndroidDriver: " + baseURL + port + minorURL);
            try {
                final Future<Object> f = executor.submit(() -> {
                    driver[0] = new AndroidDriver<>(new URL(baseURL + port + minorURL), capabilities);
                    //log("   IOSDriver started");
                    return TAG + "configureLocalAppium(): AndroidDriver started";
                });
                log(f.get(120, TimeUnit.SECONDS)); //wait 120 sec to complete
            } catch (final TimeoutException e) {
                log(TAG + "configureLocalAppium(): executor.TimeoutException()");
                e.printStackTrace();
                return null;
            } catch (final Exception e) {
                log(TAG + "configureLocalAppium(): executor.RuntimeException()");
                e.printStackTrace();
                return null;
            } finally {
                log(TAG + "configureLocalAppium(): executor.shutdown()");
                executor.shutdown();
            }
            // https://appium.io/docs/en/advanced-concepts/settings/
            ((AndroidDriver) driver[0]).setSetting(Setting.WAIT_FOR_IDLE_TIMEOUT, 500); // TODO check if helps on screen where we have looong response?
        } else {
            log(TAG + "configureLocalAppium(): IOSDriver: " + baseURL + port + minorURL);

            try {
                final Future<Object> f = executor.submit(() -> {
                    driver[0] = new IOSDriver<>(new URL(baseURL + port + minorURL), capabilities);
                    //log("   IOSDriver started");
                    return TAG + "configureLocalAppium(): IOSDriver started";
                });
                log(f.get(150, TimeUnit.SECONDS)); //wait 150 sec to complete
            } catch (final TimeoutException e) {
                log(TAG + "configureLocalAppium(): executor.TimeoutException()");
                e.printStackTrace();
                return null;
            } catch (final Exception e) {
                log(TAG + "configureLocalAppium(): executor.RuntimeException()");
                e.printStackTrace();
                return null;
            } finally {
                log(TAG + "configureLocalAppium(): executor.shutdown()");
                executor.shutdown();
            }
            // https://appium.io/docs/en/advanced-concepts/settings/
        }

        return driver[0];
    }

    public AppiumDriver getDriver(String devicePlatform, String testEnvironment, String testBuild, String appPath, String deviceName, String processArguments) {
        log(TAG + "getDriver(): appPath - " + appPath);
        long startDriverTime = System.currentTimeMillis();
        log(TAG + "getDriver(): devicePlatform: " + devicePlatform + ", deviceName: " + deviceName);

        AppiumDriver driver = configureAppiumDriver(devicePlatform, testEnvironment, testBuild, appPath, deviceName, processArguments);

        if (driver == null) {
            log(TAG + "getDriver(): FAILED to start!");
            return null;
        }
        try {
            PageFactory.initElements(new AppiumFieldDecorator(driver, Duration.ofSeconds(60)), this);
        } catch (Exception e) {
            log(TAG + "getDriver(): FAILED init timeouts!");
            e.printStackTrace();
        }

        startDriverTime = System.currentTimeMillis() - startDriverTime;
        log(TAG + "getDriver(): completed in - " + (int) startDriverTime / 1000 + " sec");
        return driver;
    }


    // CRD_VIRTUAL_CARDS,CRDT_ADD_SALARY:true,false  - pass arguments like this
    private String getAndroidFlags(String processArguments) {
        log(TAG + "getAndroidFlags() " + processArguments);
        // e.g. String to pass to capabilities as OPTIONAL_INTENT_ARGUMENTS
        // "--esa FEATURE_FLAG_KEYS CRD_VIRTUAL_CARDS,CRDT_ADD_SALARY --esa FEATURE_FLAG_VALUES true,true"

        String arguments = null;
        // parse values
        if (processArguments != null && !processArguments.isEmpty()) {
            // get Flag names
            LinkedList<String> flags = new LinkedList<>(Arrays.asList(processArguments.split(":")[0].split(",")));
            // get Flag values : true/false
            LinkedList<String> values = new LinkedList<>(Arrays.asList(processArguments.split(":")[1].split(",")));
            // prepare String to pass to capabilities
            arguments = "--esa FEATURE_FLAG_KEYS " + String.join(",", flags) + " --esa FEATURE_FLAG_VALUES" + " " + String.join(",", values);
        } else {
            log(TAG + "getAndroidFlags(): 'processArguments' - null, flags NOT set");
        }

        return arguments;
    }
}
