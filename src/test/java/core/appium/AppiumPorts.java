package core.appium;

public class AppiumPorts {
    int appium;
    // ios
    int wdaPort;
    int video;
    // android
    int bootstrap;
    int chromePort;
    int system;

    public AppiumPorts(String appium) {
        this(Integer.valueOf(appium));
    }

    public AppiumPorts(int appium) {
        this.appium = appium;
        this.wdaPort = appium + 100;
        this.bootstrap = appium + 200;
        this.video = appium + 200;
        this.chromePort = appium + 400;
        this.system = appium + 400;
    }
}
