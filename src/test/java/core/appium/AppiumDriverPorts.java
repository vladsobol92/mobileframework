package core.appium;

import org.testng.Assert;

import java.io.IOException;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.List;
import static core.Log.log;

public class AppiumDriverPorts {
    final String TAG = "      AppiumDriverPorts():       | "; // 35 + |

    private static final int MIN_PORT_NUMBER = 8000;
    private static final int MAX_PORT_NUMBER = 8999;
    private static final List<Integer> busyPorts = new ArrayList<>();

    int appium;
    // ios
    public int wda;
    int video;
    // android
    int chrome; // not in use yet
    int system;

    public AppiumDriverPorts() {
    }

    public AppiumDriverPorts(String appium) {
        this(Integer.valueOf(appium));
    }

    public AppiumDriverPorts(int appium) {
        this.appium = appium;
        // this.wda = appium + 100;
        this.wda = findFreePort();
        this.video = appium + 200;
        this.chrome = appium + 400;
        this.system = appium + 400;
    }

    private synchronized int findFreePort() {
        for (int i = MIN_PORT_NUMBER; i <= MAX_PORT_NUMBER; i++) {
            if (available(i) && !busyPorts.contains(i)) {
                busyPorts.add(i);
                log(TAG + "findFreePort(): " + i);
                return i;
            }
        }
        Assert.assertTrue(false, "findFreePort(): FAILED");
        return 0;
    }

    public synchronized void freePort(Long port) {
        log(TAG + "freePort(): " + port);
        if (port == null)
            return;
        busyPorts.remove(Integer.valueOf(port.intValue()));
        log(TAG + "freePort(): removed: " + busyPorts.toString());
    }

    private boolean available(final int port) {
        ServerSocket serverSocket = null;
        DatagramSocket dataSocket = null;
        try {
            serverSocket = new ServerSocket(port);
            serverSocket.setReuseAddress(true);
            dataSocket = new DatagramSocket(port);
            dataSocket.setReuseAddress(true);
            return true;
        } catch (final IOException e) {
            return false;
        } finally {
            if (dataSocket != null) {
                dataSocket.close();
            }
            if (serverSocket != null) {
                try {
                    serverSocket.close();
                } catch (final IOException e) {
                    // can never happen
                }
            }
        }
    }
}
