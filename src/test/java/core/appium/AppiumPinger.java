package core.appium;

import base.Base;
import io.appium.java_client.AppiumDriver;
import static core.Log.log;

public class AppiumPinger extends Base implements Runnable {

    private AppiumDriver driver;
    private boolean doPing = true;


    public AppiumPinger(AppiumDriver driver) {
        this.driver = driver;
    }

    @Override
    public void run() {
        startAppiumPing();
    }

    private void startAppiumPing() { // prevent appium stop
        System.out.println("    startAppiumPing():");
        while (doPing) {
            sleep(10000);
            log("ping...");
            log(driver.getContext());
        }
    }

    public void stopAppiumPing() {
        System.out.println("    stopAppiumPing():");
        doPing = false;
    }
}
