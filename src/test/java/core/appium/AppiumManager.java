package core.appium;

import core.ServerManager;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.*;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.AndroidServerFlag;
import io.appium.java_client.service.local.flags.GeneralServerFlag;
import io.appium.java_client.service.local.flags.IOSServerFlag;
import lombok.Getter;
import lombok.Setter;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.Socket;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import static core.Log.log;

public class AppiumManager {

    final String TAG = "    AppiumManager():                | "; // 35 + |

    @Getter
    @Setter
    public AppiumDriver driver;

    @Getter
    @Setter
    public String device;

    @Getter
    @Setter
    AppiumServiceBuilder builder;

    @Getter
    @Setter
    DesiredCapabilities capabilities;

    public AppiumDriverLocalService service;


    public AppiumManager(String device) {
        this.device = device;
    }


    public static JSONObject readCapabilitiesFile() {

        JSONParser parser = new JSONParser();

        Object obj = null;

        try {
            obj = parser.parse(new FileReader("capabilities.json"));
        } catch (FileNotFoundException noFile) {
            noFile.printStackTrace();
        } catch (IOException io) {
            io.printStackTrace();
        } catch (ParseException parserEx) {
            parserEx.printStackTrace();
        }

        JSONObject fileData = (JSONObject) obj;

        return fileData;
    }


    public AppiumManager createServiceBuilder(String device) {
        log(TAG+"createServiceBuilder():" + device);
        JSONObject deviceCapabilities = (JSONObject) readCapabilitiesFile().get(device);
        Integer appiumPort = Integer.parseInt((String) deviceCapabilities.get("appiumPort"));
        Integer debugProxyPort = getRandomNumberPortsRange(6501, 6600);

        // android
        String bootstrapPort = (String) deviceCapabilities.get("bootstrap_port");
        String chromePort = (String) deviceCapabilities.get("chrome_port");

        ServerManager.clearPort(appiumPort);
        ServerManager.clearPort(bootstrapPort);

        while (isPortInUse(appiumPort)) {
            appiumPort = appiumPort + 20;
        }
        AppiumServiceBuilder appiumServiceBuilder;
        appiumServiceBuilder = new AppiumServiceBuilder()
                .withIPAddress("127.0.0.1")
                .usingPort(appiumPort)
                .withArgument(IOSServerFlag.WEBKIT_DEBUG_PROXY_PORT,debugProxyPort.toString())
                .withArgument(GeneralServerFlag.SESSION_OVERRIDE)
                .withStartUpTimeOut(180, TimeUnit.SECONDS) // 3 min
                .withArgument(AndroidServerFlag.BOOTSTRAP_PORT_NUMBER, bootstrapPort)
                .withArgument(AndroidServerFlag.CHROME_DRIVER_PORT, chromePort)
            //    .withLogFile(LogFile)
             //   .withArgument(GeneralServerFlag.TEMP_DIRECTORY, tmpFolder)
                .withArgument(GeneralServerFlag.LOG_TIMESTAMP)
                .withArgument(GeneralServerFlag.LOG_LEVEL, "error");


        setBuilder(appiumServiceBuilder);
        log("  createServiceBuilder(): DONE");
        return this;
    }

    public void startAppiumServer() {
        log("  startAppiumServer()");
        createServiceBuilder(device);
        service = AppiumDriverLocalService.buildService(builder);
        service.start();
        log("  startAppiumServer(): DONE");
    }

    public void stopAppiumServer() {
        log("  stopAppiumServer()");
        service.stop();
        log("  stopAppiumServer(): DONE");

    }

    @Deprecated
    public AppiumDriver mobileDriver(DesiredCapabilities capabilities) {
        log(TAG+"startDriver():");
        AppiumDriver driver=null;
        long startDriverTime = System.currentTimeMillis();
        for (int i =0; i <5; i++) { // Try 5 times to start driver
            log(TAG+"startDriver(): "+i);
            try {
                if ("ios".equalsIgnoreCase(capabilities.getPlatform().toString().toLowerCase())) {
                    log(TAG+"startDriver(): ios driver");
                    driver = new IOSDriver<MobileElement>(service, capabilities);
                } else {
                    driver = new AndroidDriver<MobileElement>(service, capabilities);
                }
            } catch (Exception ex){
                ex.printStackTrace();
                log("____________________________________");
                log(TAG+"Driver creation failed after: "+i+" attempt. Retrying...");
            }
            if (driver!=null){ // If driver is started then break the cycle
                log(TAG+"startDriver(): DONE: " + (System.currentTimeMillis() - startDriverTime) / 1000 + " sec");
                log(TAG+"Driver started !!!");
            break; }

        }
        return driver;
    }



    @Deprecated
    public DesiredCapabilities capabilities(String clearDevice) {

        DesiredCapabilities capabilities = new DesiredCapabilities();

        JSONObject androidCapabilities = (JSONObject) readCapabilitiesFile().get("android");
        JSONObject iOSCapabilities = (JSONObject) readCapabilitiesFile().get("ios");

        JSONObject deviceCapabilities = (JSONObject) readCapabilitiesFile().get(device);


        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, deviceCapabilities.get("deviceName"));
       // capabilities.setCapability("platformName", (String) deviceCapabilities.get("platformName"));
        //capabilities.setCapability("platformVersion", (String) deviceCapabilities.get("platformVersion"));

        if ("ios".equalsIgnoreCase((String) deviceCapabilities.get("platformName"))) {

            Integer wdaLocalPort = Integer.parseInt(
                    (String) deviceCapabilities.get("wda_port")
            );

            while (isPortInUse(wdaLocalPort)) {
                wdaLocalPort = wdaLocalPort + 20;
            }

            if (!device.contains("sim")) {
                String path = iOSCapabilities.get("ipa").toString();
                File appDir = new File(path);

                log("APP path "+appDir);

                capabilities.setCapability(MobileCapabilityType.APP, appDir.getAbsolutePath());
                capabilities.setCapability(IOSMobileCapabilityType.XCODE_ORG_ID, iOSCapabilities.get("xcodeOrgId"));
                capabilities.setCapability(IOSMobileCapabilityType.XCODE_SIGNING_ID, iOSCapabilities.get("xcodeSigningId"));
            } else {
                String path = iOSCapabilities.get("app").toString();
                File appDir = new File(path);
                capabilities.setCapability(MobileCapabilityType.APP, appDir);
            }


            capabilities.setCapability(MobileCapabilityType.UDID, deviceCapabilities.get("udid"));
            capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, AutomationName.IOS_XCUI_TEST);

            capabilities.setCapability(IOSMobileCapabilityType.SIMPLE_ISVISIBLE_CHECK, "true");
            capabilities.setCapability(IOSMobileCapabilityType.NATIVE_WEB_TAP, "true");
            capabilities.setCapability(IOSMobileCapabilityType.SEND_KEY_STRATEGY, iOSCapabilities.get("sendKeyStrategy"));
            //capabilities.setCapability(MobileCapabilityType.LANGUAGE, "ee");
            //capabilities.setCapability(MobileCapabilityType.LOCALE, "ee_EE"); // en_EE
            //  capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, "");
            capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, MobilePlatform.IOS);
            capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, deviceCapabilities.get("platformVersion"));
            capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "120");
            // capabilities.setCapability(IOSMobileCapabilityType.SCALE_FACTOR, "0.5");
            capabilities.setCapability(IOSMobileCapabilityType.WDA_LOCAL_PORT, wdaLocalPort);
            //capabilities.setCapability(IOSMobileCapabilityType.WDA_CONNECTION_TIMEOUT, "60000"); //ms
            capabilities.setCapability(IOSMobileCapabilityType.WDA_LAUNCH_TIMEOUT, "60000");
            capabilities.setCapability("mjpegServerPort", deviceCapabilities.get("video"));
        } else if ("android".equalsIgnoreCase((String) deviceCapabilities.get("platformName"))) {

            Integer systemPort = Integer.parseInt(String.valueOf(deviceCapabilities.get("systemPort")));
            ServerManager.clearPort(systemPort);
            while (isPortInUse(systemPort)) {
                systemPort = systemPort + 20;
            }

            capabilities.setCapability("autoAcceptAlerts", (Boolean) androidCapabilities.get("autoAcceptAlerts"));
            capabilities.setCapability("autoGrantPermissions", (Boolean) androidCapabilities.get("autoGrantPermissions"));
            //capabilities.setCapability("unicodeKeyboard", (Boolean) androidCapabilities.get("unicodeKeyboard"));
           // capabilities.setCapability("resetKeyboard", (Boolean) androidCapabilities.get("resetKeyboard"));
            capabilities.setCapability(MobileCapabilityType.APP, androidCapabilities.get("app"));
            capabilities.setCapability("automationName", "uiautomator2");
            capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, deviceCapabilities.get("platformVersion"));
            capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, MobilePlatform.ANDROID);
            capabilities.setCapability("systemPort", systemPort);
            capabilities.setCapability("udid", deviceCapabilities.get("udid"));
            capabilities.setCapability(AndroidMobileCapabilityType.NO_SIGN, "true");
            capabilities.setCapability(AndroidMobileCapabilityType.DISABLE_ANDROID_WATCHERS, "true");
        }

        if (clearDevice.contains("true")) {
            log(TAG+"capabilities(): FULL RESET" );
            capabilities.setCapability(MobileCapabilityType.FULL_RESET, true);
            capabilities.setCapability(MobileCapabilityType.NO_RESET, false);
            capabilities.setCapability(IOSMobileCapabilityType.USE_NEW_WDA, true);
        } else {
            log(TAG+"capabilities(): NO RESET" );
            //soft reset
            capabilities.setCapability(MobileCapabilityType.NO_RESET, true);
            capabilities.setCapability(MobileCapabilityType.FULL_RESET, false);
        }

        //capabilities.setCapability("newCommandTimeout", 60 * 3);

        setCapabilities(capabilities);

        return capabilities;

    }

    public String getDeviceUdid(String deviceName) {
        JSONObject deviceCapabilities = (JSONObject) readCapabilitiesFile().get(deviceName);
        String platform = (String) deviceCapabilities.get("platformName");
        if (platform.equalsIgnoreCase("android")) {
            return (String) deviceCapabilities.get("deviceName");
        } else {
            return (String) deviceCapabilities.get("udid");
        }

    }

    private Integer getRandomNumberPortsRange(int min, int max) {

        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }

    private boolean isPortInUse(int port) {

        boolean result = false;
        String host = "127.0.0.1";
        // int portValue=port;
        try {
            (new Socket(host, port)).close();
            // Successful connection means the port is in use.
            result = true;
        } catch (IOException e) {
            result = false;
        }

        return result;

    }

}
