package core.appium;

import base.Base;
import io.appium.java_client.AppiumDriver;
import static core.Log.log;

public class AppiumPingerV2 extends Base implements Runnable{

    final String TAG = "    AppiumPingerV2 ():             | "; // 35 + |

    private AppiumDriver driver;
    private Thread thread = null;
    private boolean doPing = false;


    public AppiumPingerV2(AppiumDriver driver) {
        this.driver = driver;
    }

    @Override
    public void run() {
        pingAppium();
    }

    private void pingAppium() { // prevent appium stop
        log(TAG + "pingAppium(): " + doPing);
        while (doPing) {
            sleep(30000);
            if (doPing)
                try {
                    log(TAG + "pingAppium(): ping - " + driver.getCapabilities().getCapability("deviceName") + " {" + driver.getContext() + "}");
                } catch (Exception ex) {
                    stopPing();
                    break;
                }
        }
    }

    public Thread startPing() {
        synchronized (this) {
            log(TAG + "startAppiumPing():");
            if (doPing)
                log(TAG + "startAppiumPing(): ERROR: doPing = true");
            if (thread != null)
                log(TAG + "startAppiumPing(): ERROR: thread NOT null");
            if (thread == null) {
                doPing = true;
                thread = new Thread(this);
                thread.start();
            }
            return thread;
        }
    }

    public void stopPing() {
        synchronized (this) {
            log(TAG + "stopAppiumPing():");
            doPing = false;
            try {
                thread.interrupt();
            } catch (Exception e) {
                // ignore
            }
            thread = null;
        }
    }
}
