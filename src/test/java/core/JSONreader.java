package core;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileReader;
import static core.Log.*;


public class JSONreader {

    final static String TAG = "    JSONReader():                | "; // 35 + |

    public static JSONObject readJsonFromFile(String pathToFile) {
        JSONParser parser = new JSONParser();
        JSONObject fileData = null;

        try {
            fileData = (JSONObject) parser.parse(new FileReader(pathToFile));
        } catch (Exception ex) {
            log("    readJsonFromFile(): Filed to read file '" + pathToFile + "'" + "\n" + ex.getMessage());
        }
        // return null if file does not exist
        if (fileData == null) {
            log(TAG + "readJsonFromFile(): FAILED\n NO such file '" + pathToFile + "'");
            //return null;
        }
        try {
            return fileData;
        } catch (Exception ex) {
            return null;
        }
    }
}
