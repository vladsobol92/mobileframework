package core;

import core.appium.CapabilitiesReader;
import io.appium.java_client.remote.MobilePlatform;
import lombok.Getter;
import org.json.simple.JSONObject;
import org.testng.Assert;

@Getter
public class Device {


    public String appiumPort;
    public String deviceName;
    public String name;
    public DevicePlatform devicePlatform;
    public String platformVersion;
    public String udid;
    public String startType = "noReset"; // fullReset,fastReset, noReset


    public Device(String name) {
        this.name = name;
        // parce capabilities
        JSONObject deviceCapabilities = (JSONObject) CapabilitiesReader.readCapabilitiesFile().get(name);
        Assert.assertNotNull(deviceCapabilities,
                String.format("DEVICE: FAILED to init device. Make sure devise '%s' is present in capabilities.json file", deviceName));
        // set appium port
        this.appiumPort = deviceCapabilities.get("appiumPort").toString();
        // set deviceName how it is specified in capabilities
        this.deviceName = (String) deviceCapabilities.get("deviceName");
        // set platform
        setDevicePlatform(name);
        this.platformVersion = (String) deviceCapabilities.get("platformVersion");
        this.udid = (String) deviceCapabilities.get("udid");

    }

    private void setDevicePlatform(String deviceName) {
        if (deviceName.toLowerCase().contains("iphone")) {
            this.devicePlatform = DevicePlatform.IOS;
        } else {
            this.devicePlatform = DevicePlatform.ANDROID;
        }
    }

    public Device setStartType(String startType) {
        this.startType = startType;
        return this;
    }

    public enum DevicePlatform {
        ANDROID(MobilePlatform.ANDROID),
        IOS(MobilePlatform.IOS);

        String textValue;

        DevicePlatform(String textValue) {
            this.textValue = textValue;
        }

    }


}
