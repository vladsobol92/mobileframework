package core.reporter;

import core.appium.AppiumManager;
import core.ServerManager;
import lombok.Getter;
import lombok.Setter;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;
import static core.Log.log;

public class AllureReporter {

    private static final Logger logger = Logger.getLogger(AllureReporter.class);

    @Getter
    @Setter
    String reportFolder;
    @Getter
    @Setter
    String serverFolder;
    String deviceName;
    List<String> deviceList;
    String tier;

    public AllureReporter(List<String> devices, String platform){
       if (devices.size() > 1) {
           this.deviceName = platform;
       } else {
       this.deviceName = devices.get(0);}
    }


    public AllureReporter(){

       this.deviceName=System.getProperty("device");
    }

    public void createReportFolder() {
        log("    createReportFolder():");
        InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("allure.properties");
        Properties p = new Properties();
        try {
            p.load(is);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (deviceName.contains("iphone") || deviceName.toLowerCase().contains("ios")) {
            setReportFolder("/Users/" + System.getProperty("user.name") + p.getProperty("server.folder.ios") + "/" + java.time.LocalDate.now());
        } else {
            setReportFolder("/Users/" + System.getProperty("user.name") + p.getProperty("server.folder.android") + "/" + java.time.LocalDate.now());
        }

        if (!new File(getReportFolder()).exists()) {
            new File(getReportFolder()).mkdirs();
            log("    createReportFolder(): " + getReportFolder());
        }

    }

    public void createReportDirectory() {
        log("    createReportDirectory():");

        createReportFolder();
        /*
        if (System.getProperty("device") != null) {
            deviceName = System.getProperty("device");
        }
        */
        log("    createReportDirectory(): folder created: " + getReportFolder());
    }


    public void generateAllureReport() {
        log("    generateAllureReport():");

        InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("allure.properties");
        Properties p = new Properties();
        try {
            p.load(is);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String resultsDirectory;
        if (System.getProperty("allure.results.directory") == null) {
            resultsDirectory = p.getProperty("allure.results.directory");
        } else {
            resultsDirectory = System.getProperty("allure.results.directory");
        }
        log("    generateAllureReport(): results folder: " + resultsDirectory);

        ServerManager.runCommand("allure generate -c " + resultsDirectory + " -o " + getReportFolder() + "/" + deviceName);

       // ServerManager.runCommand("allure generate -c " + resultsDirectory + " -o " + getServerFolder());
        System.out.println("    generateAllureReport(): done");
        sleep(1000);
    }

    /*
    This method creates "Environment" tab in Allure reporter.
    Fo that environment.properties file is created and moved to allure results directory


    public void createEnvironment(List<String> devices){
        for (String deviceName : devices){
           createEnvironment(deviceName);
        }
    }
     */

    public void createEnvironment() {

        FileOutputStream fos;
        String path = System.getProperty("allure.results.directory");

        if (path == null || path.equals("") || path.equals(" ")) {
            java.io.InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("allure.properties");
            Properties p = new Properties();
            try {
                p.load(is);
            } catch (IOException e) {
                e.printStackTrace();
            }
            path = p.getProperty("allure.results.directory");
        }

        try {
            Properties props = new Properties();
            fos = new FileOutputStream(path + "/environment.properties");
            props.setProperty("device", deviceName);
            JSONObject deviceInfo =(JSONObject) AppiumManager.readCapabilitiesFile().get(deviceName);
            props.setProperty("OS", (String) deviceInfo.get("platformName"));
            props.setProperty("version", (String) deviceInfo.get("platformVersion"));
            props.store(fos, "See https://github.com/allure-framework/allure-app/wiki/Environment");
            fos.close();
        } catch (IOException e) {
            log("IO problem when writing allure properties file");
        }
    }



    public void sleep(int msec) {
        try {
            Thread.sleep(msec);
        } catch (Exception e) {
            // ignore
        }
    }

}



