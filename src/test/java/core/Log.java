package core;

import base.BaseTest;
import org.apache.log4j.Logger;
import org.testng.ITestContext;
import org.testng.ITestResult;

import java.time.LocalTime;

public class Log {

    private static final Logger logger = Logger.getLogger(BaseTest.class);

    public static <T> void log(T message) {
        System.out.println("[" + getCurrentTime() + "]" + "  " + message.toString());

    }

    public static <T> void logToFile(T message) {
        log(message.toString());
        logger.info(message);
    }

    public static LocalTime getCurrentTime() {
        return java.time.LocalTime.now();
    }

    public static void logResults(ITestContext context, ITestResult result) {

        float runtime = (result.getEndMillis() - result.getStartMillis()) / 1000f;

        logToFile(" RESULTS:_____________________________");
        logToFile(" TEST: " + context.getName());
        logToFile(" METHOD: " + result.getName());
        logToFile(" RUN TIME: " + runtime + " sec");
        logToFile(" ______PASSED test: " + context.getPassedTests().size());
        logToFile(" ______SKIPPED test: " + context.getSkippedTests().size());
        logToFile(" ______FAILED test: " + context.getFailedTests().size());
    }
}
