package application;

import io.appium.java_client.AppiumDriver;
import screens.example.HomeScreen;

public class TestedApplication {
    AppiumDriver driver;

    public TestedApplication(AppiumDriver driver){
        this.driver=driver;
    }

    public HomeScreen homeScreen(){
        return new HomeScreen(driver);
    }
}
