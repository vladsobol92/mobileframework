package screens.example;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.*;
import io.qameta.allure.Step;
import org.testng.Assert;

import base.BaseScreen;

import java.util.List;
import static core.Log.*;


public class StartScreen extends BaseScreen {
    final String TAG = "    StartScreen():                 | "; // 35 + |

    // TODO add Android in many items
    @HowToUseLocators(androidAutomation = LocatorGroupStrategy.ALL_POSSIBLE) // TODO FIX ME!
    @AndroidFindBy(id = "a1_title_text_view")
    @AndroidFindBy(id = "elevator_pitch_title")
    @iOSXCUITFindBy(id = "I1ElevatorPitchCarouselView")
    private List<MobileElement> mainContainer;

    // header
    @iOSXCUITFindBy(id = "pricingButton")
    private List<MobileElement> pricingButton;

    @iOSXCUITFindBy(id = "languageButton")
    private List<MobileElement> languageButton;

    // body
    @iOSXCUITFindBy(id = "ParallaxCarouselView")
    private List<MobileElement> carouselViewContainer;

    @iOSXCUITFindBy(id = "ParallaxCarouselPageView")
    private List<MobileElement> carouselContainer;

    @HowToUseLocators(androidAutomation = LocatorGroupStrategy.ALL_POSSIBLE)
    @AndroidFindBy(id = "a1_title_text_view")
    @AndroidFindBy(id = "elevator_pitch_title")
    @iOSXCUITFindBy(id = "carouselTitle")
    private List<MobileElement> carouselTitle;

    @iOSXCUITFindBy(id = "carouselFooterView")
    private List<MobileElement> carouselFooterView;

    // footer
    @iOSXCUITFindBy(id = "signUpButton")
    @AndroidFindBy(uiAutomator = "new UiSelector().textContains(\"Sign up\")")
    private MobileElement signUpButton;

    @iOSXCUITFindBy(id = "loginButton")
    @HowToUseLocators(androidAutomation = LocatorGroupStrategy.ALL_POSSIBLE)
    @AndroidFindBy(id = "a1_login_button")
    @AndroidFindBy(id = "login_text")
    private MobileElement loginButton;

    @AndroidFindBy(id = "a30_continue_button")
    private List<MobileElement> continueBtn;

    public StartScreen(AppiumDriver driver) {
        super(driver);
    }


    @Step("Select default language")
    public StartScreen selectDefaultLanguage() {
        log(TAG + "selectDefaultLanguage():");
        tap(continueBtn);
        return this;
    }

    @Step("Check 'StartScreen' loaded")
    public boolean isLoadedLanguageScreen(int sec) {
        log(TAG + "isLoaded(): " + sec);
        boolean bool;
        setWaitSeconds(sec);
        bool = !continueBtn.isEmpty();
        setDefaultWaitTime();
        return bool;
    }


    @Step("Check 'StartScreen' loaded")
    public boolean isLoaded(int sec) {
        log(TAG + "isLoaded(): " + sec);
        boolean bool;
        setWaitSeconds(sec);
        bool = !mainContainer.isEmpty();
        setDefaultWaitTime();
        return bool;
    }

    @Step("Tap 'Login' button")
    public void tapLoginButton() {
        log(TAG + "tapLoginButton():");
        Assert.assertTrue(tap(loginButton), TAG + "tapLoginButton(): FAILED");
    }

    @Step("Tap 'SignUp' button")
    public void tapSignUpButton() {
        log(TAG + "tapSignUpButton():");
        Assert.assertTrue(tap(signUpButton), TAG + "tapSignUpButton(): FAILED");
    }

}
