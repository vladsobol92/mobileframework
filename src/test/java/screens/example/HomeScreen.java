package screens.example;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.*;
import io.qameta.allure.Step;
import base.BaseScreen;

import java.util.List;

public class HomeScreen extends BaseScreen {

    AppiumDriver appiumDriver;


    static boolean isNewStartScreen;

    @HowToUseLocators(androidAutomation = LocatorGroupStrategy.ALL_POSSIBLE)
    @AndroidFindBy(id = "a1_title_text_view")
    @AndroidFindBy(id = "elevator_pitch_title")
    private List<MobileElement> title;

    @iOSXCUITFindBy(id="login")
    private MobileElement loginButtoniOS;

    @HowToUseLocators(androidAutomation = LocatorGroupStrategy.ALL_POSSIBLE)
    @AndroidFindBy(id = "a1_login_button")
    @AndroidFindBy(id = "login_text")
    private MobileElement loginButtonAndroid;

    @iOSXCUITFindBy(id="signUpWithEmail")
    @HowToUseLocators(androidAutomation = LocatorGroupStrategy.ALL_POSSIBLE)
    @AndroidFindBy(uiAutomator = "new UiSelector().textContains(\"Sign up free\")")
    @AndroidFindBy(uiAutomator = "new UiSelector().textContains(\"Sign up\")")
    private MobileElement signUp;

    @iOSXCUITFindBy(id = "examleIphoneLocatorID")
    @AndroidFindBy(id= "exampleId")
    private MobileElement exampleElement;

    public HomeScreen(AppiumDriver driver) {
        super(driver);
    }
    public boolean isScreenLoad(int sec){
        return isScreenLoad(title,sec);
    }



    @Step("Interact with example element")
    public HomeScreen interactWithElement(){
        tap(exampleElement);
        enterText(exampleElement,"test");
        return this;
    }
}
